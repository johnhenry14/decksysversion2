<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        view()->composer(['partials.header'], function ($view) {
            $currency = Whmcs::GetCurrencies([]);
                $view->with('currency', $currency);
            });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
