<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DarthSoup\Whmcs\Facades\Whmcs;
use Illuminate\Support\Facades\View;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Illuminate\Support\Facades\Input;
use Session;

class ProfileController extends Controller
{
  public function profile_update_action(){
        $clientid = session()->get('login_id');
        $results=Whmcs::GetClientsDetails(['clientid'=>$clientid]);
        return view('clientlayout.main.Myprofile',compact('results'));
    }

public function profile_update_action_update(Request $request)
{
    $clientid = session()->get('login_id');

    $profile=Whmcs::UpdateClient([
        'clientid'=>$clientid,
                    'firstname' => Input::post('firstname'),
                    'lastname' => Input::post('lastname'),
                    'email' => Input::post('email'),
		            'companyname' => Input::post('companyname'),
                    'phonenumber' => Input::post('phonenumber'),
        	        'city' => Input::post('city'),
        	        'state' => Input::post('state'),
           	        'country' => Input::post('country'),
                    'address1' => Input::post('address1'),
                    'postcode' => Input::post('postcode'),


]);
\Session::flash('message', 'Successfully updated!');
return redirect('/Myprofile');
}


}
