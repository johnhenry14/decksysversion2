<?php
namespace App\Http\Controllers;
use App\Repositories\Clients;
use App\Repositories\AcceptOrder;
use App\Repositories\AddInvoicePayment;
use App\Repositories\Invoice;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Razorpay\Api\Api;
use Cart;
use Session;
use Redirect;
use PDF;
use Crypt;
use App\Repositories\ZohoAPI;


class PaymentController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|\Illuminate\View\View
     */
    public function payWithRazorpay()
    {  
     
      $value = session()->get('login_id');
      $promo_code = "";$promo_code_id='';
      
		    if ($value != '') {
            $results = Whmcs::GetClientsDetails([
                'clientid' => $value,
            ]);
            $data = json_encode(Cart::content());
            $cartdata = json_decode($data);
		//echo "<pre>";print_r($cartdata);exit;
            $j = 0;
            $pid = array();
            $billingcycle = array();
            $package = array();
            $name = array();
           	$addons=array();
               $data_server=array();$k=0;$addons=array();
			foreach ($cartdata as $cartadd) {
                $promo_code=$cartadd->options->discount_code;
                $promo_code_id=$cartadd->options->discount_id;
                    if ($cartadd->options->type == 'server') 
                    {
                        $billingcycle[$j] = $cartadd->options->bicycle;
                        $name[$j] = $cartadd->name;
                        $pid[$j] = $cartadd->options->gid;
                        $os[$j]=$cartadd->options->os;
                       
                            $dedicated_server=explode('-',$cartadd->options->add_on_os);
                            $dedicated_server_1=explode('_',$dedicated_server[2]);
                            
                            $data_server[$dedicated_server_1[0]]=$dedicated_server_1[1];

                            $dedicated_server1=explode('-',$cartadd->options->add_on_db );
                            $dedicated_server_2=explode('_',$dedicated_server1[2]);
                            
                            $data_server[$dedicated_server_2[0]]=$dedicated_server_2[1];
                        $j++;
                    }
                    else
                    {
                        $billingcycle[$j] = $cartadd->options->bicycle;
                        $name[$j] = $cartadd->name;
                        $pid[$j] = $cartadd->options->gid;
                        $os[$j]=$cartadd->options->os;

                        $dedicated_server=explode('-',$cartadd->options->add_on_os);
                            $dedicated_server_1=explode('_',$dedicated_server[2]);
                            
                            $data_server[$dedicated_server_1[0]]=$dedicated_server_1[1];

                            $dedicated_server1=explode('-',$cartadd->options->add_on_db );
                            $dedicated_server_2=explode('_',$dedicated_server1[2]);
                            
                            $data_server[$dedicated_server_2[0]]=$dedicated_server_2[1];
                            
                            if($cartadd->options->add_on_st!='')
                            {
                            $dedicated_server2=explode('-',$cartadd->options->add_on_st);
                            $dedicated_server_3=explode('_',$dedicated_server2[2]);
                            
                            $data_server[$dedicated_server_3[0]]=$dedicated_server_3[1];

                            $dedicated_server3=explode('-',$cartadd->options->add_on_dp );
                            $dedicated_server_4=explode('_',$dedicated_server3[2]);
                            
                            $data_server[$dedicated_server_4[0]]=$dedicated_server_4[1];

                            $dedicated_server4=explode('-',$cartadd->options->add_on_cp );
                            $dedicated_server_5=explode('_',$dedicated_server4[2]);
                            
                            $data_server[$dedicated_server_5[0]]=$dedicated_server_5[1];

                            $dedicated_server5=explode('-',$cartadd->options->add_on_dc );
                            $dedicated_server_6=explode('_',$dedicated_server5[2]);
                            
                            $data_server[$dedicated_server_6[0]]=$dedicated_server_6[1];
                           
                            }
                    }
                    $addons[$k]=base64_encode(serialize($data_server)); 
                    $k++;
                }
               
	/* Add Order to WHMCS */
       $order_details = Whmcs::AddOrder([
                'clientid' => $value,
                'paymentmethod' => 'razorpay',
                'pid' => $pid,
                'billingcycle' => $billingcycle,
                'configoptions'=>$addons,
                'promocode'=>$promo_code,
                'affid' => $promo_code_id
            ]);
            $order_id = $order_details['orderid'];
            $invoiceid = $order_details['invoiceid'];
            return view('main.checkout', compact('results', 'cartdata', 'order_id', 'invoiceid', 'order_details'));
               } else {
            session()->put('checkout', 'true');
            return redirect('login');
        }
    }

    public function payment_success(Request $request)
    {

        $data = json_encode(Cart::content());
        $cartdata = json_decode($data);
        $value = session()->get('login_id');
        $payment_id = Input::get('payment_id');
        $order_id = Input::get('order_id');
        $invoiceid = Input::get('invoiceid');
       
    /* AddInvoice Payment */
    $dataobject=new Invoice();
        $paymentinvoice=$dataobject->GetInvoice($invoiceid);

        //echo "<pre>";print_r($paymentinvoice);exit;
    $invoice_items=[];$i=0;
		foreach($paymentinvoice['items']['item'] as $key => $invoice)
		{
			$invoice_item[$i]["item_id"]="1791721000000074013";
			$invoice_item[$i]["description"]=$invoice['description'];
			$invoice_item[$i]["rate"]=$invoice['amount'];
			$invoice_item[$i]["quantity"]=1;
			$invoice_item[$i]["discount_amount"]=0;
			$invoice_item[$i]["discount"]=0;
			$i++;
		}
		
		$customer_fields[0]['customfield_id']=1;
		$customer_fields[0]['value']=$order_id;
		$customer_fields[1]['customfield_id']=2;
		$customer_fields[1]['value']=$invoiceid;
		$currency=session()->get('currency');
        $zoho_invoice['customer_id']="1791721000000077001";
        $zoho_invoice['currency_code']=$currency;
		$zoho_invoice['gateway_name']="Razorpay";
		$zoho_invoice['line_items']=$invoice_item;
		$zoho_invoice['additional_field1']=$invoiceid;
		$zoho_invoice['additional_field2']=$value;
		$zoho_invoice['additional_field3']=$order_id;

        $zohoapi=new ZOHOAPI();
        $zohoapi_invoice=$zohoapi->PostInvoice(json_encode($zoho_invoice));
        
        $dataobject=new AddInvoicePayment();
        $results=$dataobject->AddInvoicePayment();

	/* Accept Order */

        $dataobject=new AcceptOrder();
        $results1=$dataobject->AcceptOrder();
	
	/* Getclients */

        $dataobject=new Clients();
        $clientdetails=$dataobject->GetClients();

        $result2 = Whmcs::GetOrders([]);
        $request->session()->forget('promo_apply');

       //echo "<pre>";print_r($cartdata);exit;
      Cart::destroy();
	  return redirect('payment_invoice/'.Crypt::encrypt($order_id).'/'.Crypt::encrypt($invoiceid));
    }

    public function invoice_details($id,$iid)
	{
        //echo $id
    $order_id=Crypt::decrypt($id);
	$invoice_id=Crypt::decrypt($iid);
	$data = json_encode(Cart::content());
    $cartdata = json_decode($data);
	/* Get Invoice */

        $dataobject=new Invoice();
        $paymentinvoice=$dataobject->GetInvoice($invoice_id);
		//echo "<pre>";print_r($invoice_id);exit;

	/* Get Clients*/

        $dataobject=new Clients();
        $clientdetails=$dataobject->GetClients();

 	$result2 = Whmcs::GetOrders([]);
	
	return view('main.payment_success', compact('paymentinvoice', 'clientdetails','order_id','cartdata','result2'));
	
	}
}