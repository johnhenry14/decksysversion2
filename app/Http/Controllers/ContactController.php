<?php
Namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Notifications\InboxMessage;
use App\Http\Controllers\Controller;
use App\Http\Requests\ContactFormRequest;
use App\Admin;
use App\Mail\ContactEmail;
use Mail;
use Session;


Class ContactController extends Controller
{
	 public function create()
    {
        return view('clientlayout.main.contact');
    }

	public function store(Request $request)
  {

    $contact = [];
    $contact['name'] = $request->get('name');
    $contact['email'] = $request->get('email');
    $contact['msg'] = $request->get('msg');
	//dd($contact);
	
	Mail::to(config('mail.from.address'))->send(new ContactEmail($contact));

    // Mail delivery logic goes here
	\Session::flash('Your message has been sent!');

    

    return redirect('contact');

  }
}