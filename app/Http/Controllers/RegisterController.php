<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use WHMCS\Database\Capsule;
use Illuminate\Support\Facades\Input;

class RegisterController extends Controller
{

    public function insertform(){
        return view('Register');
    }
    
    
    public function insert(){
        
        $custom_fields=Input::get('customfield');
        $user=Whmcs::AddClient([
            'firstname' => Input::get('firstname'),
            'lastname' => Input::get('lastname'),
            'email' => Input::get('email'),
            'address1' => Input::get('address1'),
            'city' => Input::get('city'),
            'state' => Input::get('state'),
            'postcode' => Input::get('postcode'),
            'country' => Input::get('country'),
            'phonenumber' => Input::get('phonenumber'),
            'password2' => Input::get('password2'),
            'address2' => Input::get('address2'),
            'companyname'=>Input::get('companyname'),
            'currency'=>Input::get('currency'),
            'customfield[1]' => $custom_fields[1]


        ]);
        if($user['result']=='error'){


            return redirect('/Register?error');
        }
        else
        {
            return redirect('/login?success');
        }
    }
}

