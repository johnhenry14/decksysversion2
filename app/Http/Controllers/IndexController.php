<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DarthSoup\Whmcs\Facades\Whmcs;
use Illuminate\Support\Facades\View;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;

class IndexController extends Controller
{
   
    public function get(){
        $clientid = session()->get('login_session');
            if($clientid ==true)
            {
                $clientid = session()->get('login_id');
		            $data = Whmcs::GetClientsProducts([
                        'clientid'=>$clientid,
                        'stats' =>true
                    ]);        
                $response = Whmcs::GetInvoices([ 'userid'=>$clientid]);
                return view('clientlayout.main.index',compact('data','response'));
	        }
            elseif($clientid=='')
            {
                return redirect('login');
            }		  
        }     

        public function getdomains(){
            $clientid = session()->get('login_session');
            if($clientid=true){
            $domain=Whmcs::GetClientsDomains(['clientid'=>$clientid]);
            return view('clientlayout.main.Mydomains',compact('domain'));        
            }
            elseif($clientid=='') {
                return redirect('login');
            }
            }       

            public function show(){
                    $clientid = session()->get('login_id');
                    if($clientid == true){
                    $value = Whmcs::GetClientsProducts(['clientid'=>$clientid]);
                    return view('clientlayout.main.Services',compact('value'));
                
                    }
                    elseif($clientid == '')
                    {
                        return redirect('login');
                    }
                        
                    }
             

                    public function orders(){
                        $clientid = session()->get('login_id');
                        if($clientid==true){
                        $orders=Whmcs::GetOrders(['userid'=>$clientid]);
                //echo "<pre>";print_r($orders);exit;
                        return view('clientlayout.main.Orders',compact('orders'));	
                        }
                        elseif($clientid=='')
                        {
                            return redirect('login');
                        }
                        
                    }

                   
            
                            public function payment(){
                                $clientid = session()->get('login_id');
                                if($clientid==true){
                                
                        $response=Whmcs::GetInvoices(['userid'=>$clientid]);
                        
                                return view('clientlayout.main.Invoices',compact('response'));		
                                }
                                elseif($clientid==''){
                                    return redirect('login');
                                }
                                
                            }






}