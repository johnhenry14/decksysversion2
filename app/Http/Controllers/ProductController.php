<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\View;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;
use Illuminate\Support\Facades\Input;
use Torann\GeoIP\Facades\GeoIP;
use Session;
use App\Repositories\ZohoAPI;

class ProductController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function show()
    {

        $currency=session()->get('currency');
        $ip= \Request::ip();
        $var = geoip()->getLocation($ip);
        if(isset($currency) && $currency=='INR')
        {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
        }
        else if($currency=='USD')
        {
            session()->put('currency','USD');
            session()->put('currency_sign', 'usd');
        }
        else
        {
            if($var['currency']=='INR')
            {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
            }
            else
            {
                session()->put('currency','USD');
                session()->put('currency_sign', 'usd');
            }
        }
        $products = Whmcs::GetProducts([]);
        $currency = Whmcs::GetCurrencies([]);
        return view('main.VPS', compact('products','currency'));
    }


    public function home()
    {

        $currency=session()->get('currency');
        $ip= \Request::ip();
        $var = geoip()->getLocation($ip);
        if(isset($currency) && $currency=='INR')
        {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
        }
        else if($currency=='USD')
        {
            session()->put('currency','USD');
            session()->put('currency_sign', 'usd');
        }
        else
        {
            if($var['currency']=='INR')
            {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
            }
            else
            {
                session()->put('currency','USD');
                session()->put('currency_sign', 'usd');
            }
        }
        $products = Whmcs::GetProducts([]);
        $currency = Whmcs::GetCurrencies([]);
        return view('main.index', compact('products','currency'));
    }

    public function show2()
    {
        $currency=session()->get('currency');
        $ip= \Request::ip();
        $var = geoip()->getLocation($ip);
        //print_r($var);exit;
        if(isset($currency) && $currency=='INR')
        {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
            session()->put('country', 'india');
        }
        else if($currency=='USD')
        {
            session()->put('currency','USD');
            session()->put('currency_sign', 'usd');
            session()->put('country', 'United States');
            
        }
        else
        {
            if($var['currency']=='INR')
            {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
            session()->put('country', 'india');
            }
            else
            {
                session()->put('currency','USD');
                session()->put('currency_sign', 'usd');
                session()->put('country', 'United States');
            }
        }
        $currency = Whmcs::GetCurrencies([]);
        $products = Whmcs::GetProducts([]);
        $this->createzogo_item($products);
        return view('main.DedicatedServer', compact('products','currency'));
    }


    public function check_already_exiting_zogo($id)
    {
        $zohoapi=new ZOHOAPI();
        $zohoapi_product=$zohoapi->Items();
        $items=json_decode($zohoapi_product);
        $flags=0;
        foreach($items->items as $product)
        {
               if($product->cf_whmcs_id==$id)
               {
                   $flags=1;
               }
        }
        if($flags==0)
        {
            return 0;
        }
        else
        {
            return 1;
        }
    }


    public function createzogo_item($products)
    {
        $zohoapi=new ZOHOAPI();
          $array=array();
          foreach($products['products']['product'] as $value)
          {
           // $check=$this->check_already_exiting_zogo($value['pid']);
            //if($value['gid']=="6")
           // {
                //$array['vendor_id']=$value['pid'];
                $array['name']=$value['name'];
                $array['rate']=$value['pricing']['INR']['monthly'];
                $array['sku']=$value['pid'];
                $zohoapi_invoice=$zohoapi->CreateItem(json_encode($array));
           // }
                     
          }
    }

    public function CurrencyChange(Request $request)
    {
        $currency= Input::get('currency');
        if($currency=='INR')
            {
            session()->put('currency', 'INR');
            session()->put('currency_sign', 'inr');
            }
            else
            {
                session()->put('currency','USD');
                session()->put('currency_sign', 'usd');
            }
        //session()->put('currency',$currency);
        return response()->json(['success' => $currency]);

    }

    public function ajax_cp(Request $request)
    {
           print_r(Input::get('type'));exit;
    }


    


}
