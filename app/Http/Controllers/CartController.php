<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Cart;
use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\Input;
use DarthSoup\Whmcs\Facades\Whmcs;
use DarthSoup\Whmcs\WhmcsServiceProvider;
use App\Repositories\Promotion;
use App\Repositories\ZohoAPI;



class CartController extends Controller
{
    public function addToCart(){
    return Cart::content();
    }	

    public function addCart_dedicated(Request $request)
    {

		$name = $request->input();
		// echo"<pre>";print_r($name);exit;
		$gid=$name['gid'];
		$os=$name['os'];
		$hosting=explode('-',$name['os']);
	    $database=explode('-',$name['db']);
        $hosting1=$name['name1'];
		$description=$name['description'];
		$price=$name['price'];
	
		$check=$this->get_promocode_check($gid);
		//echo"<pre>";print_r($check);exit;

		if($check!=0)
		{
			
			$discount_id=$check['id'];
			$discount_code=$check['code'];
			$discount_type=$check['type'];
			$discount_value=$check['value'];
			if($discount_code!=''){
				\Session::flash('message_promo');
			}
			
			if($discount_type=='Percentage')
			{
				$discount_amount=($price*$discount_value)/100;
			}
			else
			{
				$discount_amount=($price-$discount_value);
			}
		}
		else
		{
			$discount_id='';
			$discount_code='';
			$discount_type='';
			$discount_value='';
			$discount_amount=0;
		}
	    $msetupfee=0;
        $response=Cart::add(['id'=>str_random(50) ,'name' => $hosting1, 'quantity' => 1, 'price' => $price-$discount_amount,'options' => ['price1'=>$price,'package' => 'Dedicated Server','bicycle'=>'','gid'=>$gid,'description'=>$description,'name'=>$hosting1,'msetupfee'=>$msetupfee,'type'=>'server','os'=>$hosting[1],'db'=>$database[1],'discount_id'=>$discount_id,'discount_code'=>$discount_code,'discount_price'=>$discount_amount,'discount_percentage'=>$discount_value,'add_on_os' => $name["os"],'add_on_db' => $name["db"],'add_on_st'=>"",'add_on_dp'=>"",'add_on_cp'=>"" ]]);
		$data=json_encode($response);
		$cartdata=json_decode($data);
		
		return redirect('cart');
	}
	public function addCart_vps(Request $request)
    {
		$name = $request->input();
		//echo "<pre>";print_r($name);exit;
		if($name['operating_system']!='' && $name['service_type']!='' && $name['data_backup']!='' && $name['data_centers'] !='')
			{
			$gid=$name['vps_server_id'];
			$hosting=explode('-',$name['operating_system']);
			$database=explode('-',$name['database']);
			$service_type=explode('-',$name['service_type']);
			$data_backup=explode('-',$name['data_backup']);
			$control_panel=explode('-',$name['control_panel']);
			$hosting1=$name['vps_server'];
			$description=$name['vps_server'];
			$price=$name['total_p'];
			$data_centers=explode('-',$name['data_centers']);
			$check=$this->get_promocode_check($gid);

		if($check!=0)
		{
			$discount_id=$check['id'];
			$discount_code=$check['code'];
			$discount_type=$check['type'];
			$discount_value=$check['value'];
			if($discount_code!=''){
				\Session::flash('message_promo');
			}
			if($discount_type=='Percentage')
			{
				$discount_amount=($price*$discount_value)/100;
			}
			else
			{
				$discount_amount=($price-$discount_value);
			}
		}
		else
		{
			$discount_id='';
			$discount_code='';
			$discount_type='';
			$discount_value='';
			$discount_amount=0;
		}
			$msetupfee=0;
			$response=Cart::add(['id'=>str_random(50) ,'name' => $hosting1, 'quantity' => 1, 'price' => $price-$discount_amount, 'options' => ['price1'=>$price,'package' => 'VPS Server','bicycle'=>'','gid'=>$gid,'description'=>$description,'name'=>$hosting1,'msetupfee'=>$msetupfee,'type'=>'vpsserver','os'=>$hosting[1],'db'=>$database[1],'st'=>$service_type[1],'dp'=>$data_backup[1],'dc'=>$data_centers[1],'cp'=>$control_panel[1],'discount_id'=>$discount_id,'discount_code'=>$discount_code,'discount_price'=>$discount_amount,'discount_percentage'=>$discount_value,'add_on_os' => $name["operating_system"],'add_on_db' => $name["database"],'add_on_st' => $name["service_type"],'add_on_dp' => $name["data_backup"],'add_on_cp'=>$name['control_panel_post'],'add_on_dc'=>$name['data_centers']]]);
			
			$data=json_encode($response);
			$cartdata=json_decode($data);
			
			return redirect('cart');
		}
		else
		{
			\Session::Flash('os_error');
			\Session::Flash('database_error');
			\Session::Flash('serviceType_error');
			\Session::Flash('backup_error');
			\Session::Flash('customfield1');
			return redirect('VPS');
		}
		
	}
	public function get_promocode_check($pid)
	{
		$promocodes=Whmcs::GetPromotions();
		
        $flags=array();
		foreach($promocodes['promotions']['promotion'] as $codes)
		{
			 $promo_code=$codes['code'];
			 $applies_to=explode(',',$codes['appliesto']);
			 //echo"<pre>";print_r($applies_to);exit;
			 if(in_array($pid,$applies_to))
			 {
				 $flags=$codes;
			 }
		}
		if(count($flags)==0)
		{
		return 0; 
	} 
	else 
	{ return $flags;}
	
	}
	
    public function cart_view()
    {
		
        $data=json_encode(Cart::content());
		$cartdata=json_decode($data);
	
		if(empty($cartdata)){
			\Session::Flash('message_empty');
		}
		//echo "<pre>";print_r($cartdata);exit;
	
		
		return view('main.cart',compact('cartdata'));
    }
	
	public function apply_promo(Request $request)
    {
		$promocode_code = session()->get('promo_apply');
		$name = $request->input();
		//echo "<pre>";print_r($name);exit;
		$code=$name['code'];
		$code_flags=0;
		$p_id='';$discount_id=0;
		$discount_value=0;
		$discount_type='';
		$Object = new Promotion();
        $response = $Object->GetPromotion();
		if($promocode_code != 'existing')
		{
		foreach($response['promotions']['promotion'] as $value)
		{ 
			if($value['code']==$code)
			{
				$code_flags=1;
				$discount_id=$value['id'];
				$p_id=$value['requires'];
				$discount_value=$value['value'];
				$discount_type=$value['type'];
			}
			
		}
		if($code_flags==1)
		{
			$p_id_array=explode(',',$p_id);
			$flags=0;
			$data=json_encode(Cart::content());
			$cartdata=json_decode($data);

			
		
		foreach($cartdata as $key => $value)
		{
					if(in_array($value->options->gid,$p_id_array))	
						{
							$flags=1;
							$price=$value->price;
								if($discount_type=='Percentage')
									{
									   $discount_p=(($price*$discount_value)/100);
									   $price=$price;
									   
									   $value->options->discount_code = $code;
									   $value->options->discount_price = $discount_p;
									   $value->options->discount_percentage = $discount_value;
									   $rowid=$value->rowId;
									  
			                           $response=Cart::add(['id'=>str_random(50) ,'name' => $value->name, 'quantity' => $value->quantity, 'price' => $value->options->price1-$discount_p, 'options' => ['price1'=>$value->options->price1,'package' => 'Dedicated Server','bicycle'=>'','gid'=>$value->options->gid,'description'=>$value->options->description,'name'=>$value->options->name,'msetupfee'=>$value->options->msetupfee,'type'=>'vpsserver','os'=>$value->options->os,'db'=>$value->options->db,'discount_id'=>$discount_id,'discount_code'=>$code,'discount_price'=>$discount_p,'discount_percentage'=>$discount_value,'add_on_db'=>$value->options->add_on_db,'add_on_os'=>$value->options->add_on_os,'add_on_st'=>$value->options->add_on_st,'add_on_dp'=>$value->options->add_on_dp,'add_on_cp'=>$value->options->add_on_cp]]);
									   
									   Cart::remove($rowid);	
									   //$optins=array('discount_code'=>$discount_code,'discount_price'=>$discount_amount,'discount_percentage'=>$discount_value);
									   //Cart::update($value->rowId, ['price' => $price,'options[discount_code]' =>$code,'options'=>$optins ]);
									}
						}
						
		}
		
		
		if($flags==1)
		{
			session()->put('promo_code', $code);
			session()->put('promo_apply', 'existing');
			\Session::flash('message');
			return redirect('cart?success');
		}
		else
		{
			session()->put('promo_code', '');
			session()->put('promo_apply', '');
			\Session::flash('message_error2');
			return redirect('cart?error');
		}
		}
		else{
			
			session()->put('promo_code', '');
			\Session::flash('message_error');
			return redirect('cart?error');
		}
		}
		else
		{
			session()->put('promo_code', '');
			\Session::flash('message_error1');
			return redirect('cart?error');
		}
		}
	
	public function delete_promo()
	{
		$data=json_encode(Cart::content());
		$cartdata=json_decode($data);
		
		foreach($cartdata as $key => $value)
		{
			$rowid=$value->rowId;
			
				$response=Cart::add(['id'=>str_random(50) ,'name' => $value->name, 'quantity' => $value->quantity, 'price' => $value->options->price1,  'options' => ['price1'=>$value->options->price1,'package' => 'Dedicated Server','bicycle'=>'','gid'=>$value->options->gid,'description'=>$value->options->description,'name'=>$value->options->name,'msetupfee'=>$value->options->msetupfee,'type'=>'vpsserver','os'=>$value->options->os,'db'=>$value->options->db,'discount_code'=>'','discount_id'=>'','discount_price'=>0,'discount_percentage'=>0,'add_on_db'=>$value->options->add_on_db,'add_on_os'=>$value->options->add_on_os,'add_on_st'=>$value->options->add_on_st,'add_on_dp'=>$value->options->add_on_dp,'add_on_cp'=>$value->options->add_on_cp]]);
			
			
			Cart::remove($rowid);				
		}
		session()->put('promo_apply', '');
		return redirect('cart');
		 
	}

	

    public function cart_remove(Request $request)
    {
        $request->session()->forget('promo_apply');
		session()->put('promo_code', '');
		Cart::remove($request->rowid);
		\Session::flash('message_delete');
        return redirect('cart');

    }
    
    public function destroy(){
        Cart::destroy();
        return redirect('cart');
    }
}