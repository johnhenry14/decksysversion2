<?php
namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Controllers\ProductController;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use WHMCS\Database\Capsule;
use Illuminate\Support\Facades\Input;
use Mail;
use Session;
use Cart;
use App\Repositories\ZohoAPI;

class LoginController extends Controller
{
    public function signin(){
        $value = session()->get('login_session');
        if($value =='' )
        {
            return view('main.login');
        }
        else
        {
            return redirect('/clientlayout.main.index');
        }
    }

    public function forgotpassword_action(Request $request)
    {
        $email= Input::get('email');
        $check = Whmcs::GetClientsDetails([
            'email' => Input::get('email')
        ]);

        if($check['result']=='error'){
            $var =$check['message'];
            return redirect('/forgotpassword?error');
        }
        else
        {
            $encrypt=base64_encode(base64_encode($email));
            $url=url('/').'/reset_password?secrete='.$encrypt;
            Mail::send('main.mail', ['url' => $url,'email'=>$email], function ($message) use ($email)
            {
                $message->from('do-not-reply@decksys.com', 'Decksys do-not-reply');
			    $message->to($email)->subject('Decksys Account Reset Password Link!');;
            });
            return redirect('/forgotpassword?success');
        }
    }

    public function reset_password()
    {
        //echo $encrypt=base64_encode(base64_encode('sharmila@maktoinc.com'));
        $email=base64_decode(base64_decode($_GET['secrete']));
        return view('main.reset_password',compact('email'));

    }

    public function reset_password_action(Request $request)
    {
        $email= Input::get('email');
        $login = Whmcs::UpdateClient([
            'clientemail' => Input::get('email'),
            'password2' => Input::get('password2'),
        ]);
        if($login['result']=='error'){
            $encrypt=base64_encode(base64_encode($email));
            return redirect('/reset_password?secrete='.$encrypt);
        }
        else
        {
		    return redirect('/login?success');
        }
    }

    protected function signin_action(Request $request){
        $login = Whmcs::validatelogin([
            'email' => Input::get('email'),
            'password2' => Input::get('password2'),
        ]);

        if($login['result']=='error'){
            $var =$login['message'];
            return redirect('/login?error');
        }

        $results=Whmcs::GetClientsDetails([
            'clientid' => $login['userid'],
        ]);

        //echo "<pre>";print_r($results);exit;
        $value = session()->get('checkout');
        $request->session()->put('login_session', 'true');
        $request->session()->put('login_id',$login['userid'] );
        $request->session()->put('login_user_name',$results['fullname'] );
        $request->session()->put('companyname',$results['companyname'] );
        $request->session()->put('email',$results['email'] );
        $request->session()->put('address1',$results['address1'] );
        $request->session()->put('address2',$results['address2'] );
        
        // Zoho Contact Details
        $contact_array['contact_name']=$results['fullname'];
        $contact_array['company_name']=$results['companyname'];
        $contact_array['first_name']=$results['firstname'];
        $contact_array['last_name']=$results['lastname'];
        $contact_array['contact_persons'][0]['first_name']=$results['firstname'];
        $contact_array['contact_persons'][0]['last_name']=$results['lastname'];
        $contact_array['contact_persons'][0]['email']=$results['email'];
        $contact_array['contact_persons'][0]['phone']=$results['phonenumber'];
        $contact_array['phone']=$results['phonenumber'];
         //$contact_array['billing_address']=$results['address1'];
         //$contact_array['shipping_address']=$results['address2'];
         
       // print_r($contact_array['email']);exit;
        $zohoapi=new ZOHOAPI();
        $zohoapi_invoice=$zohoapi->CreateContact(json_encode($contact_array));
        $zoho_book=json_decode($zohoapi_invoice);
        //print_r($zoho_book);exit;

        $zoho_book_id=$zoho_book->contact->contact_id;
        //print_r($zoho_book_id);exit;

        $user=Whmcs::UpdateClient([
        'clientid'=>$login['userid'],
        'customfield[18]' => $zoho_book_id
        ]);
        //print_r($user);exit;
    
        if($login['result'] == 'success') {
            if($value=='true')
            {
                $request->session()->forget('checkout');
                return redirect('/checkout');
            }
            else
            {
                $request->session()->forget('checkout');
                return redirect('/clientlayout.main.index');
            }
        }
    }

    public function signout(Request $request) {
        $request->session()->forget('checkout');
        $request->session()->forget('login_session');
        $request->session()->forget('login_id');
        Cart::destroy();
        return redirect('/login');
    }

    
}