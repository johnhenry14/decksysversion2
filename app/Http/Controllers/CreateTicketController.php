<?php

namespace App\Http\Controllers;

use App\Http\Controllers\ProductController;
use Illuminate\Http\Request;

use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;

use WHMCS\Database\Capsule;
use Illuminate\Support\Facades\Input;
use Crypt;


class CreateTicketController extends Controller
{
    /**
     * insertform
     *
     * @return void
     */
    public function insertform(){
        return view('RaiseTicket');
        }

        /**
         * create
         *
         * @return void
         */
        public function create(Request $request){

	$clientid = session()->get('login_id');
if($clientid == ''){
 return redirect('/signin');   
}
$file = $request->file('attachments');
//print_r($_FILES);exit;
if(isset($file))
{
$file_array=array('name'=>$_FILES['attachments']['name'],'data'=>base64_encode(file_get_contents($_FILES['attachments']['tmp_name'])));
}
else
{
	$file_array=[];
}
if($clientid == true){
 $ticket=Whmcs::OpenTicket([
			'clientid'=>$clientid,
                        'deptid' => Input::get('deptid'),
                        'subject' => Input::get('subject'),
                        'message' => Input::get('message'),
						'serviceid' => Input::get('serviceid'),
						'markdown'=>true,
                        'priority' => Input::get('priority')]);
						
						//'attachments'=>base64_encode(json_encode(array($file_array)))]);
						
						//print_r($ticket);exit;
            \Session::flash('message', 'Ticket Created Successfully!');
            return redirect('/RaiseTicket');
}
else{
	return redirect('signin');
}
            
}


/* Delete Tickets */

public function DeleteTicket(Request $request){
        $clientid = session()->get('login_id');
        if(isset($_GET['ticket_id']) && $_GET['ticket_id']!='')
        {   
       $clientid = session()->get('login_id');
       $ticket_id=Crypt::decrypt($_GET['ticket_id']);
        $results=Whmcs::DeleteTicket([
            'clientid'=>$clientid,
	    'ticketid' => $ticket_id
    ]); 
    return redirect('/Tickets');    
        }
        else
        {
            return redirect('/Tickets');
        }

}



/* Update Tickets */
/*
public function show_tickets(Request $request){
        $clientid = session()->get('login_id');
        $results=Whmcs::UpdateTicket([
            'clientid'=>$clientid,
	    'ticketid' => '$ticketid'
	]);     

}
*/




/* Show Tickets */

public function show_tickets(Request $request){
        $clientid = session()->get('login_id');
		if($clientid ==true)
        {
		
        $results=Whmcs::GetTickets([
            'clientid'=>$clientid,
	]);     
return view('clientlayout.main.Tickets',compact('results'));
}
else
            {
                return redirect('signin');
            }

}
    
/* Reply Ticket */


public function add_reply(Request $request){
$clientid = session()->get('login_id');
$message=Input::get('message');
$ticketid=Input::get('ticket_id');
$reply = Whmcs::AddTicketReply([
	'clientid'=>$clientid,
	'ticketid' => $ticketid,
	'message' => $message,
	
]);
return redirect('/ReplyTicket?ticket_id='.Crypt::encrypt($ticketid));

}


public function reply_ticket(Request $request){
 if(isset($_GET['ticket_id']) && $_GET['ticket_id']!='')
 {   
$clientid = session()->get('login_id');
$ticket_id=Crypt::decrypt($_GET['ticket_id']);
$reply = Whmcs::GetTicket([
    'clientid'=>$clientid,
    'ticketid' =>$ticket_id
]);
return view('clientlayout.main.ReplyTicket',compact('reply','clientid','ticket_id'));
 }
 else
 {
    return redirect('/Tickets');
 }


}



 



}
