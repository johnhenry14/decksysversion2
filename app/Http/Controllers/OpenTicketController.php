<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;


use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;

class OpenTicketController extends Controller
{
public function clientproduct(Request $request){
   
$clientid = session()->get('login_id');
if($clientid==true)
{
	$openticket = Whmcs::GetClientsProducts([
	'clientid'=>$clientid,
	'markdown' => true
	]);
return view('clientlayout.main.RaiseTicket',compact('openticket'));
}
else
{
	return redirect('login');
}

 }
}
