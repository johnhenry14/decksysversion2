<?php
namespace App\Repositories;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class ZOHOAPI
{
	public $organization_id=686315456;


	public $token="5dbda9ff8826d8e1936befbfec7d622d";
	
	public $url="https://books.zoho.com/api/v3/";
    function PostInvoice($invoice, $send = false)
    {
        $data = array(
			'authtoken' 		=> $this->token,
			'JSONString' 		=> $invoice,
			"organization_id" 	=> $this->organization_id
		);
		$ch = curl_init($this->url."invoices/");
		curl_setopt_array($ch, array(
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_RETURNTRANSFER => true
		));
		$invoice = curl_exec($ch);
		//print_r($invoice);exit;
		return $invoice;
	}

	function CreateItem($items, $send = false)
    {
        $data = array(
			'authtoken' 		=> $this->token,
			'JSONString' 		=> $items,
			"organization_id" 	=> $this->organization_id
		);
		$ch = curl_init($this->url."items/");
		curl_setopt_array($ch, array(
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_RETURNTRANSFER => true
		));
		$output = curl_exec($ch);
		return $output;
	}

	


	function CreateContact($contact, $send = false)
    {
        $data = array(
			'authtoken' 		=> $this->token,
			'JSONString' 		=> $contact,
			"organization_id" 	=> $this->organization_id
		);
		$ch = curl_init($this->url."contacts/");
		curl_setopt_array($ch, array(
			CURLOPT_POST => 1,
			CURLOPT_POSTFIELDS => $data,
			CURLOPT_RETURNTRANSFER => true
		));
		$output = curl_exec($ch);
		return $output;
	}
	
	function Contacts()
	{
		//https://books.zoho.com/api/v3/contacts?page=2&per_page=25

		
		$ch = curl_init($this->url."contacts?authtoken=".$this->token."&organization_id=".$this->organization_id);
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => array("Content-Type: application/json")
		));
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		$output = curl_exec($ch);
		//print_r($output);exit;
		return $output;
	}
	function Items()
	{
		//https://books.zoho.com/api/v3/contacts?page=2&per_page=25

		
		$ch = curl_init($this->url."items?authtoken=".$this->token."&organization_id=".$this->organization_id);
		curl_setopt_array($ch, array(
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_HTTPHEADER => array("Content-Type: application/json")
		));
		//curl_setopt($ch, CURLOPT_HTTPHEADER, array("Content-Type: application/json"));
		$output = curl_exec($ch);
		
		return $output;
	}
}