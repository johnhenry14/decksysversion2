<?php
namespace App\Repositories;
use DarthSoup\Whmcs\Facades\Whmcs;
use Darthsoup\Whmcs\WhmcsServiceProvider;
use Session;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class Invoice
{
    function GetInvoice($id)
    {
        $value = session()->get('login_id');
        // $payment_id = Input::get('payment_id');
        // $order_id = Input::get('order_id');
        //  $invoiceid = Input::get('invoiceid');

        $paymentinvoice = Whmcs::GetInvoice([
            'clientid' => $value,
            'invoiceid' => $id,
        ]);

        return $paymentinvoice;
    }
}