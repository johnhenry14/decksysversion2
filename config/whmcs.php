<?php

return [

    /*
    |--------------------------------------------------------------------------
    | HTTP Driver
    |--------------------------------------------------------------------------
    |
    | Supported: "guzzlehttp"
    */

    'driver' => 'guzzlehttp',

    /*
    |--------------------------------------------------------------------------
    | API Credentials
    |--------------------------------------------------------------------------
    |
    | Enter the unhashed variant of your password if you use 'password' as 'auth_type'
    |
    | Supported auth types': "api", "password"
    |
    */

    'auth_type' => 'password',

    'apiurl' => 'http://35.154.23.150/includes/api.php',

    'api' => [
        'identifier' => 'YOUR_API_IDENTIFIER',
        'secret' => 'YOUR_API_SECRET',
    ],

    'password' => [
        'username' => 'admin',
        'password' => 'candy345',
    ],

    /*
    |--------------------------------------------------------------------------
    | ResponseType
    |--------------------------------------------------------------------------
    |
    | Supported auth types': "json", "xml"
    |
    */

    'responsetype' => 'json',
];
