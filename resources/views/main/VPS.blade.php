@extends('layouts.master')

@section('title')
    Decksys | VPS
@endsection
@section('content')

@php 
$currency1=session()->get('currency');
$currency_sign=session()->get('currency_sign');
$price=0;
$gid=0;
@endphp


<section class="specificationSection pt-4 pb-4" >
  <form class="targets" action="{{route('cart.add_vps_server')}}" method="post"  >
  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
    <div class="container-fluid">
        <div class="row">
            <div class="col-lg-12 text-center p-3"><h2><b>Choose the SSD VPS Server</b></h2></div>
              
            </div>
        </div>
        <div class="container-fluid">
            <div class="row no-gutters ">
                <div class="col-sm-6 col-md-4 col-lg-2 offset-lg-1" style="border:1px solid #e3e3e3">
            <div class="vpsHolder">
                <div class="vpsContent">
                    <div class="textHeader text-center text-white  p-2">
                        <h4>VPS 0.5GB</h4>
                    </div>
                    <div class="textContent text-center bg-white p-2">
                   
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 0.5GB")
                                            @foreach($key['pricing'] as $k=>$total)
                                                @if($k== $currency1)
                                                @php 
                                                    $price=$total['monthly'];
                                                    $gid=$key['pid'];
                                                @endphp
                                                    <h4 class="subHeader p-3"><strong><i class="fa fa-{{$currency_sign}}"></i>{{$total['monthly']}}/mo</strong></h4>
                                                @endif
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach                 
                     
                        <p><strong>0.5GB</strong> RAM</p>
                        <p><strong>16GB</strong> SSD Disk</p>
                        <p><strong>2TB</strong> Bandwidth</p>
                        <p><strong>1</strong> Core</p>
                        <label><input class="checkbox" id="myCheck" type="radio" name="ssdProduct" value="{{$price}}" checked onclick="myFunction({{$price}},1,'VPS 0.5GB',{{$gid}})">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
            <div class="vpsHolder">
                <div class="vpsContent">
                    
                    <div class="textHeader text-center text-white  p-2">
                        <h4>VPS 1GB</h4>
                    </div>
                    <div class="textContent text-center bg-white  p-2">
                    
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 1GB")
                                            @foreach($key['pricing'] as $k=>$total)
                                            @if($k== $currency1)
                                            @php 
                                                $price=$total['monthly'];
                                                $gid=$key['pid'];
                                            @endphp
                                            <!-- <span class="currency"></span> -->
                                            <h4 class="subHeader p-3"><strong><i class="fa fa-{{$currency_sign}}"></i>{{$total['monthly']}}/mo</strong></h4>
                                           
                                            @endif
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach 
                        <p><strong>1GB </strong>RAM</p>
                        <p><strong>20GB </strong>SSD Disk</p>
                        <p><strong>3TB </strong>Bandwidth</p>
                        <p><strong>4 </strong>Core</p>
                        <label><input class="checkbox" id="myCheck1" type="radio" name="ssdProduct" value="{{$price}}" onclick="myFunction({{$price}},2,'VPS 1GB',{{$gid}})">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3"> 
            <div class="vpsHolder">
                <div class="vpsContent">                    
                    <div class="textHeader text-center text-white p-2">
                        <h4>VPS 2GB</h4>
                    </div>
                    <div class="textContent text-center bg-white p-2 ">
                  
                    @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 2GB")
                                            @foreach($key['pricing'] as $k=>$total)
                                            @if($k== $currency1)
                                            @php 
                                                $price=$total['monthly'];
                                                $gid=$key['pid'];
                                            @endphp
                                            <h4 class="subHeader p-3"><strong><i class="fa fa-{{$currency_sign}}"></i>{{$total['monthly']}}/mo</strong></h4>
                                            @endif
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach 
                        <p><strong>2GB </strong>RAM</p>
                        <p><strong>22GB </strong>SSD Disk</p>
                        <p><strong>1TB </strong>Bandwidth</p>
                        <p><strong>4 </strong>Core</p>
                        <label><input class="checkbox" id="myCheck2" type="radio" name="ssdProduct" value="{{$price}}" onclick="myFunction({{$price}},3,'VPS 2GB',{{$gid}})">
                        </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
                <div class="vpsHolder">
                    <div class="vpsContent">
                        <div class="textHeader text-center text-white p-2">
                            <h4>VPS 4GB</h4>
                        </div>
                        <div class="textContent text-center bg-white   p-2">
                      
                        @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 4GB")
                                            @foreach($key['pricing'] as $k=>$total)
                                            @if($k== $currency1)
                                            @php 
                                                $price=$total['monthly'];
                                                $gid=$key['pid'];
                                            @endphp
                                            <h4 class="subHeader p-3"><strong><i class="fa fa-{{$currency_sign}}"></i>{{$total['monthly']}}/mo</strong></h4>
                                            @endif
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach 
                            <p><strong>4GB </strong>RAM</p>
                            <p><strong>20GB </strong>SSD Disk</p>
                            <p><strong>1TB </strong>Bandwidth</p>
                            <p><strong>4 </strong>Core</p>
                            <label><input class="checkbox" id="myCheck3" type="radio" name="ssdProduct" value="{{$price}}" onclick="myFunction({{$price}},4,'VPS 4GB',{{$gid}})">
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2" style="border:1px solid #e3e3e3">
                    <div class="vpsHolder">
                        <div class="vpsContent">
                            <div class="textHeader text-center text-white p-2">
                                <h4>VPS 8GB</h4>
                            </div>
                            <div class="textContent text-center bg-white p-2 ">
                          
                            @foreach($products['products']['product'] as $key)
                                    @if($key['gid'] == "16")
                                        @if($key['name'] == "VPS 8GB")
                                            @foreach($key['pricing'] as $k=>$total)
                                            @if($k== $currency1)
                                            @php 
                                                $price=$total['monthly'];
                                                $gid=$key['pid'];
                                            @endphp
                                            <h4 class="subHeader p-3"><strong><i class="fa fa-{{$currency_sign}}"></i>{{$total['monthly']}}/mo</strong></h4>
                                            @endif
                                            @endforeach
                                        @endif
                                    @endif
                    @endforeach 
                                <p><strong>8GB </strong>RAM</p>
                                <p><strong>24GB </strong>SSD Disk</p>
                                <p><strong>1TB </strong>Bandwidth	</p>
                                <p><strong>4 </strong>Core</p>
                                <label><input class="checkbox" id="myCheck4" type="radio" name="ssdProduct" value="{{$price}}" onclick="myFunction({{$price}},5,'VPS 8GB',{{$gid}})">
                                </label>
                            </div>
                        </div>
                    </div>
                </div>
             
    </div>
</div>
</section>
<section class="container-fluid " style="background-color: #fff">
<div class="Header text-center">

    <h2 class="p-4 headingStyle" >Select your Option</h2>
    <p id="msg" style="color:red"></p>
</div>
<div class="row">
    <div class="col-lg-2"><h5 class="text-center optionHeading">Operating System</h5>
        @php  $i=0; @endphp
        <div class="form-group p-4">
            <select class="form-control select_option" name="operating_system" id="os" data-type="operating_system">
                <option value=''>Select</option>
          @php $price_array=array(); @endphp
             @foreach($products['products']['product'] as $value)
				@if($value['gid'] == "16") 
                	@if($value['name'] == "VPS 0.5GB")
				        @foreach($value['configoptions']['configoption'] as $configure)                                  
	    						@if($configure['name']=='VPS_OS')										
		    						@foreach($configure['options']['option'] as $options)
                                      @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                        @foreach($price_array as $key_p => $pricing) 
                      				        <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" >{{$options['name']}}</option>
                                        @endforeach	
                                    @endforeach																															
					    		@endif
					        @endforeach	                            
                        
					@endif
		    	@endif
			@endforeach	
                                </select>
                                <div class='errorText error_field' >
                                @if (Session::has('os_error'))
            <p style="text-align: center;color: red;font-size: 12px;">Select an Operating System.</p>
	
        @endif 
        </div>
    
                                </div>	
            
    </div>
    <div class="col-lg-2"><h5 class="text-center optionHeading">Control Panel</h5>
        <input type="hidden" name="control_panel_post" id="control_panel_post" /> 
        <input type="hidden" name="control_panel" id="control_panel" value="0.00-None-12_38" /> 
            <div class="form-group p-4">
                <select class="form-control select_option" name="control_panel" id="selectCP" disabled data-type="control_panel">
                    <option value=''>Select</option>
                      {{$selected=''}} 
                        @foreach($products['products']['product'] as $value)
                            @if($value['gid'] == "16") 
                                @if($value['name'] == "VPS 0.5GB")
                                        @foreach($value['configoptions']['configoption'] as $configure)                                  
                                            @if($configure['name']=='VPS_ControlPanel')										
                                                @foreach($configure['options']['option'] as  $options)
                                                @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                       @foreach($price_array as $key_p => $pricing) 
                                         @if($options['name']=='None') @php $selected='selected'; @endphp @endif
			    				        <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' {{$selected}} id="{{$options['name']}}"  data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" >{{$options['name']}}</option>
                                        @endforeach	
                                                @endforeach																															
                                            @endif
                                        @endforeach	                            
                                    
                                @endif
                            @endif
                        @endforeach	
                </select>
                </div>
            
    </div> 
     <div class="col-lg-2"><h5 class="text-center optionHeading">Databases</h5>
       <div class="form-group p-4">
        <select class="form-control select_option" name="database" id="" data-type="database">
        <option value=''>Select</option>
             @foreach($products['products']['product'] as $value)
				@if($value['gid'] == "16") 
                	@if($value['name'] == "VPS 0.5GB")
                            @foreach($value['configoptions']['configoption'] as $configure)                                  
	    						@if($configure['name']=='VPS_Databases')										
		    						@foreach($configure['options']['option'] as  $options)
                                    @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                       @foreach($price_array as $key_p => $pricing) 
			    				        <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" selected>{{$options['name']}}</option>
                                        @endforeach	
                                    @endforeach																															
					    		@endif
					        @endforeach	                            
					@endif
		    	@endif
			@endforeach	
            </select>
                                
                                </div>	
        
    </div> 

    <div class="col-lg-2"><h5 class="text-center optionHeading">Service Type</h5>
    
        <div class="form-group p-4">
        <select class="form-control select_option" name="service_type" id="" data-type="service_type">
        <option value=''>Select</option>
             @foreach($products['products']['product'] as $value)
				@if($value['gid'] == "16") 
                	@if($value['name'] == "VPS 0.5GB")
						                                     
                            @foreach($value['configoptions']['configoption'] as $configure)                                  
	    						@if($configure['name']=='VPS_ServiceType')										
		    						@foreach($configure['options']['option'] as $options)
                                    @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                       @foreach($price_array as $key_p => $pricing) 
                                         
			    				        <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" >{{$options['name']}}</option>
                                        
                                        @endforeach	
                                    @endforeach																															
					    		@endif
					        @endforeach	                            
                        
					@endif
		    	@endif
			@endforeach	
                                </select>
                                <div class='errorText error_field' >
                                @if (Session::has('serviceType_error'))
            <p style="text-align: center;color: red;font-size: 12px;">Select service type.</p>
	        @endif 
        </div>
                                </div>	
            
    </div> 
    <div class="col-lg-2"><h5 class="text-center optionHeading">Data Backup</h5>
    
        <div class="form-group p-4">
        <select class="form-control select_option" name="data_backup" id="" data-type="data_backup">
        <option value=''>Select</option>
             @foreach($products['products']['product'] as $value)
				@if($value['gid'] == "16") 
                	@if($value['name'] == "VPS 0.5GB")
								                                     
                            @foreach($value['configoptions']['configoption'] as $configure)                                  
	    						@if($configure['name']=='VPS_DataBackup')										
		    						@foreach($configure['options']['option'] as $options)
                                    @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                       @foreach($price_array as $key_p => $pricing) 
                                         
			    				        <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" >{{$options['name']}}</option>
                                        
                                        @endforeach	
                                    @endforeach																															
					    		@endif
					        @endforeach	                            
                        
					@endif
		    	@endif
			@endforeach	
                                </select>
                                <div class='errorText error_field' >
                                @if (Session::has('backup_error'))
            <p style="text-align: center;color: red;font-size: 12px;">Do you need backup.</p>
	        @endif 
        </div>
                                </div>	
            
    </div> 


    <div class="col-lg-2"><h5 class="text-center optionHeading">Data centers</h5>
    
    <div class="form-group p-4">
    <select class="form-control select_option" name="data_centers" id="" data-type="data_center">
    <option value=''>Select</option>
         @foreach($products['products']['product'] as $value)
            @if($value['gid'] == "16") 
                @if($value['name'] == "VPS 0.5GB")
                        @foreach($value['configoptions']['configoption'] as $configure)                                  
                            @if($configure['name']=='VPS_Datacenter')										
                                @foreach($configure['options']['option'] as $options)
                                @php $price_array[0]=$options['pricing'][$currency1]; @endphp
                                   @foreach($price_array as $key_p => $pricing) 
                                     
                                    <option value='{{$pricing['monthly']}}-{{$options['name']}}-{{$configure['id']}}_{{$options['id']}}' data-price="{{$pricing['monthly']}}" data-name="{{$options['name']}}" >{{$options['name']}}</option>
                                    
                                    @endforeach	
                                @endforeach																															
                            @endif
                        @endforeach	                            
                    
                @endif
            @endif
        @endforeach	
                            </select>
                            <div class='errorText error_field' >
                            @if (Session::has('backup_error'))
        <p style="text-align: center;color: red;font-size: 12px;">Select a Datacenter.</p>
        @endif 
    </div>
                            </div>	
        
</div> 
   
    
</div>

        
        <div class="container-fluid pricing">
             
                <div class="row no-gutters pricingcontent">
                       
                        <div class="field col-12 col-sm-12 col-md-3 col-lg-3 p-3 offset-md-9 offset-lg-9">
                            <h6 class="totalPrice">Price<strong>
                            <input type="hidden" id="type_vpsid" name="vps_server_id" value="223" />
                            <input type="hidden" id="type_vps" name="vps_server" value="VPS 0.5GB" />
                            <input type="hidden" id="type_checkbox" />
                            <input type="hidden" id="price_number" />
                            <input type="hidden" id="total_price" name="total_p"  />
                            <!-- <span class="currency">$</span><span class="price-val">5.00</span> -->
                            <!-- <p>Display some text when the checkbox is checked:</p> -->
                            @foreach($products['products']['product'] as $key)
                            @if($key['gid'] == "16")
                                @if($key['name'] == "VPS 0.5GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="currency"></span>  
                                    <span class="price-val" id="text1" > <i class="fa fa-{{$currency_sign}}"></i> {{$var=$total['monthly']}}/mo</span> 
                                     @endif
                                    @endforeach
                                    @elseif($key['name'] == "VPS 1GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="price-val" id="text2" style="display:none"> <i class="fa fa-{{$currency_sign}}"></i> {{$var1=$total['monthly']}}/mo</span>   
                                    @endif
                                    @endforeach
                                    @elseif($key['name'] == "VPS 2GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="price-val" id="text3" style="display:none"> <i class="fa fa-{{$currency_sign}}"></i>{{$var2=$total['monthly']}}/mo</span>   
                                    @endif
                                    @endforeach
                                    @elseif($key['name'] == "VPS 4GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="price-val" id="text4" style="display:none"> <i class="fa fa-{{$currency_sign}}"></i>{{$var3=$total['monthly']}}/mo</span>   
                                    @endif
                                    @endforeach
                                    @elseif($key['name'] == "VPS 8GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="price-val" id="text5" style="display:none"> <i class="fa fa-{{$currency_sign}}"></i>{{$var4=$total['monthly']}}/mo</span>   
                                    @endif
                                    @endforeach
                                    @elseif($key['name'] == "VPS 16GB")
                                    @foreach($key['pricing'] as $cur => $total)
                                     @if($cur==$currency1)
                                    <span class="price-val" id="text6" style="display:none"> <i class="fa fa-{{$currency_sign}}"></i>{{$var5=$total['monthly']}}/mo</span>   
                                    @endif
                                    @endforeach


                               @endif
                            @endif
                        @endforeach

                            </strong>

                            <!-- <span class="ssd-price-term">/mo</span> <h2> -->
                            <button type="submit" class="btn btn-info ml-3">BUY NOW</button>
                        </div>
                       
</div>

</div>
</form>

        </section>
<section class="featuresSection pt-4 pb-4" >
<div class="container Vps">

<div class="text-center ">
    <h2><strong>FEATURES</strong></h2>
</div>
<div class="container-fluid pt-3">
    <table class="table " style="border-color:1px solid red">
        <thead>
            <tr>
                <th class="bg-info pl-5" style="width:450px;">Features</th>
                <th class="pl-5" style="width:450px;">SSD-VPS</th>
            </tr>
        </thead>
        <tbody>
            <tr class="bg-active">
                <td class="pl-5">Price (from)</td>
                <td class="pl-5">$5</td>
            </tr>      
            <tr>
                <td class="pl-5">Web site hosting</td>
                <td class="pl-5"><img src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features2" >
                </td>
            </tr>
            <tr>
                <td class="pl-5">Application hosting</td>
                <td class="pl-5"><img src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features2" >
                </td>
            </tr>
        </tbody>
    </table>
</div>
<div>
</section>


<script>

var checkBox = document.getElementById("myCheck");
$('#type_checkbox').val(1);
$('#price_number').val(checkBox.value);

var price_format={
    "operating_system":0,"service_type":0,"database":0,"data_backup":0,"control_panel":0
}
function set_price(type,price)
{
    price_format[type]=price;
    console.log(price_format);
    return 1;
}
function cal_price(price)
{
  var total=parseFloat(price)
  + parseFloat(price_format['operating_system']) + parseFloat(price_format['control_panel']) + parseFloat(price_format['service_type'])
  + parseFloat(price_format['database']) + parseFloat(price_format['data_backup']);
   return total;
}


function disabled_selectbox_control_panel(id)
{
    if(id==1)
    {
        $("#selectCP option:eq(1)").prop("disabled", false);
        $("#selectCP option:eq(0)").prop("disabled", true);
        $("#selectCP option:eq(2)").prop("disabled", true);
        $("#selectCP option:eq(3)").prop("disabled", true);
    }
    else if(id==2)
    {
        $("#selectCP option:eq(1)").prop("disabled", true);
        $("#selectCP option:eq(0)").prop("disabled", true);
        $("#selectCP option:eq(2)").prop("disabled", false);
        $("#selectCP option:eq(3)").prop("disabled", true);
    }
    else if(id==3)
    {
        $("#selectCP option:eq(1)").prop("disabled", true);
        $("#selectCP option:eq(0)").prop("disabled", true);
        $("#selectCP option:eq(2)").prop("disabled", true);
        $("#selectCP option:eq(3)").prop("disabled", false);
    }
}

function set_control_panel(type,id)
{
//     $.ajax({
// type:'POST',
// url:'{{route('ajax_control_panel')}}',
// data:{type:type, id:id, _token:$('meta[name="csrf-token"]').attr('content')},
// success:function(data){
//    alert(data.success);
// }
// });

    if(id!=1)
    {
        
        $('#msg').html('');
    if(type.search('Ubuntu') >=0 )
    {
       
        $("#selectCP option").removeAttr("selected");
        $("#selectCP option").prop("disabled");
        disabled_selectbox_control_panel(1);
       
        $("#selectCP option:eq(1)").prop("selected", true);
        
    }
    else if (type.search('Cent') >=0)
    {
        
        $("#selectCP option").removeAttr("selected");
        $("#selectCP option").prop("disabled");
        disabled_selectbox_control_panel(1);
        $("#selectCP option:eq(1)").prop("selected", true);
        
    }
    else if (type.search('Windows') >=0 )
    {
        
        $("#selectCP option").removeAttr("selected");
        $("#selectCP option").prop("disabled");
        disabled_selectbox_control_panel(2);
        $("#selectCP option:eq(2)").prop("selected", true);
        
    }
    var element = $('#selectCP').find('option:selected'); 
        var value = element.attr("data-price"); 
    var type_v=$('#selectCP').attr('data-type');
    set_price(type_v,value);
    }
    else
    {
       
       
        $("#selectCP option").removeAttr("selected");
        $('#msg').html('Cpanel and Plesk needs atleast 1GB');
        $("#selectCP option").prop("disabled");
        disabled_selectbox_control_panel(3);
        //$("#selectCP option:eq(3)").prop("disabled", false);
        $("#selectCP option:eq(3)").prop("selected", true);
        var value=$('#selectCP option:selected').attr('data-price');
    var type_v=$('#selectCP').attr('data-type');
    set_price(type_v,value);
        
    }
    
                    
}

$('.select_option').change(function(e){
    $('#control_panel_post').val($(this).val())
    var id=$('#type_checkbox').val();
    var price=$('#price_number').val();
    console.log(price);
    var value=$('option:selected', this).attr('data-price');
    var type=$(this).attr('data-type');
    var name=$('option:selected', this).attr('data-name');
    set_price(type,value);
    $('.price-val').hide();
    $('#text'+id).show();
    var total=cal_price(price);
    $('#total_price').val(total);
    $('#text'+id).html('<i class="fa fa-{{$currency_sign}}"></i>'+total+'/mo');
    if(type="operating_system"){ set_control_panel(name,id); }
});

function myFunction(price,id,title,pid) {
    $('#type_checkbox').val(id);
    $('#price_number').val(price);
    $('#type_vps').val(title);
    $('#type_vpsid').val(pid);
    
    var total=cal_price(price);
    $('.price-val').hide();
    $('#text'+id).show();
    $('#total_price').val(total);
    $('#text'+id).html('<i class="fa fa-{{$currency_sign}}"></i>'+total+'/mo');
    $('#os').prop('selectedIndex', 0);
    
    if(id==1)
    {
        $('#msg').html('Cpanel and Plesk needs atleast 1GB');
        disabled_selectbox_control_panel(3);
        $('#selectCP').attr('disabled','true');
        //$('#selectCP').html("<option value='None' selected id='None' data-price='0.00' data-name='None' >None</option>");
    }
    else
    {
       $('#msg').html('');
       $('#selectCP').attr('disabled','true');
        //$('#selectCP').removeAttr('disabled');
    }
}

</script>
@endsection
