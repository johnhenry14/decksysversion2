@extends('layouts.mastercart')
@section('title')
    DeckSys | Dedicated Servers, VPS, Coimbatore India
@endsection
@section('content')
@php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
@endphp
<div id="pageloader">
   <img src="{{asset('img/loader.gif')}}" alt="processing..." />
</div>

<section class="smeHostingSection">

<div class="container">
    <div class="row">
        <div class="col-sm-12 col-md-12 col-lg-12">
        <h1 class="text-center p-3" style="color:steelblue">Review & Pay</h1>
        </div>
    </div>
</div>
</section>

<div class="container">

            <div style="margin-top:20px;padding: 50px;background-color: #e6e6ea;">
            <div class="row">
                <div class="col-xs-12 col-lg-6 col-sm-6">
                    <strong>FROM:</strong><br>
            
                    <h4>Makto Technology Pvt Ltd</h4>
                    <p><a class="text-dark" href="http://www.maktoinc.com">www.maktoinc.com</a></p>
                    <p>B2, First Floor, Rathinam Technical Campus<br>
                        Eachanari, Pollachi Main Road,<br>
                        Coimbatore 641 021, Tamilnadu, India.<br>
                        Email: info@maktoinc.com
                    </p>
                    <p>CALL : +91-73391 88891<br></p>
                </div>
                <div class="col-xs-12 col-lg-6 col-sm-6 text-right">
                    <strong>TO:</strong>
                    @foreach($results as $key => $value)
                        @if($key == 'fullname')
                            <h4>{{$value}}</h4>
                        @elseif($key == 'companyname')
                            {{$value}}<br>
                        @elseif($key == 'phonenumber')
                            Phone: {{$value}}<br>
                        @elseif($key == 'address1')
                            {{$value}}<br>
                        @elseif($key == 'city')
                            {{$value}}<br>
                        @elseif($key == 'state')ol
                            {{$value}}<br>
                        @elseif($key == 'postcode')
                            {{$value}}<br>
                        @elseif($key == 'email')
                            Email: {{$value}}<br>
                            </p>
                        @endif
                    @endforeach
                </div>
            </div>
        </div>

        <div class="line"></div>
                        <table class="table">
                            <thead>
                            <tr>
                            <td>S.No</td>
                                <td colspan="1">Order Details</td>
                                <td>Cost</td>
                                <td>Actions</td>
                            </tr>
                            </thead>
                            <tbody> 
                             @php 
                             $tax_total=0;
                             $sub_total=0;
                             $sub_total1=0;$sub_total2=0;
                             $discount_code='';
                             $discount_percentage=0;
                             $discount_amount=0;
                             $count=Cart::count();
                             $i=0;
                             @endphp
                            @foreach($cartdata as $key=>$value)    
                            @php $i++; @endphp
                                <form action="{{route('cart.remove')}}" method="POST">								 
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="rowid" value="{{$value->rowId}}" />
                                    <tr>
                                    <td>{{$i}}</td>

                                    <td>
                                    @if($value->options->package == 'Dedicated Server')
                                        <b>{{$value->options->description}}</b><br>
                                        <b>1. Operating System - </b> {{$value->options->os}}<br>
                                        <b>2. Database - </b>{{$value->options->db}} 
                                        @endif
                                        @if($value->options->package == 'VPS Server')
                                        <b>{{$value->options->description}}</b><br>
                                        <b>1. Operating System - </b>{{$value->options->os}}<br>
                                        <b>2. Database - </b>{{$value->options->db}}<br>
                                        <b>3.Service Type - </b>{{$value->options->st}}<br>
                                        <b>4. DataBackup - </b>{{$value->options->dp}}<br>
                                        <b>5. Datacenter - </b>{{$value->options->dc}}<br>
                                        <b>6. ControlPanel - </b>{{$value->options->cp}}<br>
                                       
                                        @endif
                                    </td>                                                                                                                                                                 
                                    <td style="padding-top: 15px;">
                                    <i class="fa fa-{{$currency_sign}}"></i>
                                      {{round($value->options->price1,2)}}  </td>
                                    <td><!-- <button class="btn btn-info btn-sm"><i class="fa fa-refresh"></i></button> -->
                                        <button type="submit" class="center-block">X</button>                                                                         
                                    </td>
                                    </tr>
                                    <?php 
                                    $sub_total2=$value->options->price1;
                                    if($value->options->discount_code!=''){ 
                                        $discount_code=$value->options->discount_code;
                                       $discount_percentage=$value->options->discount_percentage;
                                    } 
                                        ?>
                                    
                                
							        <?php  $tax_total=$tax_total+$value->tax; $sub_total=$sub_total+$value->subtotal;?>
                                    @endforeach
                                </form>
                            </tbody>
                            <tfoot>
                            @php if($discount_code!=''){ @endphp
                            <tr>                                
                                        <td colspan="2" class="text-right"> Discount ( {{ $discount_code }}, {{ $discount_percentage }} %)</td>                             
                                        <td style="padding-top: 15px;">
                                        <i class="fa fa-{{$currency_sign}}"></i> 
                                       
                                        {{$sub_total1=round(($sub_total2*$discount_percentage)/100,2)}} </td>
                                        <td></td>
                                    </tr> 
                                    <tr>                                
                                        <td colspan="2" class="text-right"> Sub Total</td>                             
                                        <td style="padding-top: 15px;"> 
                                        <i class="fa fa-{{$currency_sign}}"></i>
                                         {{$sub_total2-$sub_total1}}  </td>
                                        <td></td>
                                    </tr> 
                            @php } @endphp
                            @if($currency=="INR")
                             <tr>                                
                                <td colspan="2" class="text-right"> GST @ 18.00%</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{round($g=$tax_total,2)}}</td>
                                <td></td>
                            </tr> 
                            @else
                            @endif
                            @if($currency=="INR")
                            <tr>                                
                                <td colspan="2" class="text-right"> Total</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{$g+$sub_total}}</td>
                                <td></td>
                            </tr>
                            @else
                            <tr>                                
                                <td colspan="2" class="text-right"> Total</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{round($sub_total,2)}}</td>
                                <td></td>
                            </tr>
                            @endif
                            </tfoot>
                        </table>
					</div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <div class="container">
        <div class="row">
            <div class="col-md-4 col-md-offset-4">
                @if($message = Session::get('error'))
                    <div class="alert alert-danger alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Error!</strong> {{ $message }}
                    </div>
                @endif
                {!! Session::forget('error') !!}
                @if($message = Session::get('success'))
                    <div class="alert alert-info alert-dismissible fade in" role="alert">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">×</span>
                        </button>
                        <strong>Success!</strong> {{ $message }}
                    </div>
                @endif
                {!! Session::forget('success') !!}
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-8"></div>
            <div class="col-lg-4 text-right" style="padding: 20px;"><button id="rzp-button1" class="btn btn-primary btn-lg text-center">Pay With Razorpay</button></div>
            <div class="col-lg-offset-4"></div>
        </div>
    </div>

    
    <form method="post" id="razorpay_form" action="{{route('payment.success')}}">
        <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <input type="hidden" id="payment_id" name="payment_id" />
        <input type="hidden" id="order_id" name="order_id" value="<?=$order_id;?>" />
        <input type="hidden" id="order_id" name="invoiceid" value="<?=$invoiceid;?>" />
    </form>

    <script src="https://checkout.razorpay.com/v1/checkout.js"></script>
    <script>
        var options = {
            "key": "rzp_test_pu7EjHhQTbrbqk",
            //"amount":"1000",
            
            "currency":"{{$currency}}",
            "amount": @if($currency=='INR')
                            "{{($g+$sub_total2-$sub_total1)*100}}"
                        @else
                            "{{($sub_total2-$sub_total1)*100}}"
                        @endif, // 2000 paise = INR 20
            "name": "Decksys",
            "description": "Purchase Description",
            "image": "img/logo/logo-decksys.png",
            "handler": function (response){
                $('#payment_id').val(response.razorpay_payment_id);
                //$('#order_id').val();// php order_id values
                $('#razorpay_form').submit();

            },
            "prefill": {
                "name": "<?=$results['fullname'];?>",
                "email": "<?=$results['email'];?>",
                "contact": "<?=$results['phonenumber'];?>"
            },
            "notes": {
                "address": "<?=$results['address1'].''.$results['address2'];?>"
            },
            "theme": {
                "color": "#F37254"
            }
        };
        var rzp1 = new Razorpay(options);

        document.getElementById('rzp-button1').onclick = function(e){
            rzp1.open();
            e.preventDefault();
        }
    </script>
    <script>
function setElementsDisabled() {
  $('#newLayerName').keyup(function() {
      $('#btnSaveLayer').prop('disabled', $(this).val().length == 0);
  })
}
setElementsDisabled()

$(".clear").click(function() {
  $("#newLayerName").val('').trigger("keyup");
})
</script>
<script type="text/javascript">
    $('.load_button').submit(function() {
        $('#gif').show(); 
        return true;
    });

    $(document).ready(function(){
  $("#razorpay_form").on("submit", function(){
    $("#pageloader").fadeIn();
  });//submit
});
</script>

@endsection