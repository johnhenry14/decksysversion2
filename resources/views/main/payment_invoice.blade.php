
<!-- content -->
@section('title')
DeckSys |  Dedicated Servers &  VPS Hosting, Coimbatore India
@endsection
@section('content')
<?php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
//echo "<pre>";print_r($currency);exit;
?>
<div id="content" class="container" role="main">
    <div class="app-content-body ">
        <div class="bg-light lter b-b wrapper-md hidden-print">
            <a href class="btn btn-sm btn-info pull-right" onClick="window.print(); return false"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a>
   <a href="http://www.maktoinc.com" target="_blank">
<img src="{{asset('images/makto.png')}}"  alt="Makto" class="decksysBrandLogo"></a></div>
        </div>
        <div class="wrapper-md">
            <div>
               
                <div class="row">
 <div class="col-xs-6">
 <img src="{{asset('images/decksys.png')}}"  alt="Makto" class="decksysBrandLogo" style="margin-top:20px;"></div>
                    <div class="col-xs-6 text-right">
                    <?php echo "<pre>";print_r($cartdata);exit; ?>
			@foreach($paymentinvoice as $key => $invoice)
				@if($key == 'status')
                                	<h1 class="font-initial">{{$invoice}}</h1>
                                @endif
			@endforeach
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : {{$invoice}}</h5>
                            @endif
                            @if($key == 'invoiceid')
                                <h5>Invoice Id : {{$invoice}}</h5>
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            TO:
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                @endif
                            @endforeach
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 text-right">
                            Pay To:
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com<br>
				GST No: 33AAKCM8132C1ZQ<br>
                                CALL : +91 - 73391 88891
                            </p>
                        </div>
                    </div>
                </div>


      <p class="m-t m-b">
                    @foreach($result2['orders']['order']['0'] as $key => $value)
                        @if($key =='date')
                        <p class="m-t m-b">Order date: {{$value}}<br>
                            @endif
                        @if($key =='status')
                    Order status: <span class="label bg-success">{{$value}}</span><br>
                        @endif
                    @endforeach
                    Order ID: {{$order_id}}
               </p>
                <div class="line"></div>
                <table class="table table-striped bg-white b-a">
                    <thead>
                    <tr>
                        {{--<th style="width: 60px">QTY</th>--}}
                        <th class="font-initial">DESCRIPTION</th>
                        <th></th>
                        <th style="width: 90px" class="font-initial">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>

                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
                       
			<tr>
			@for($key = 0; $key < 100; $key++)
                        @endfor
  			<td>{{$invoice['description']}}</td> <td></td>
<td><i class="fa fa-inr"></i> {{$invoice['amount']}}</td>
   			</tr>

                        @endforeach
                
                    <tr>
                        <td class="text-right" colspan="2">Sub Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    

                    
@foreach($clientdetails as $key => $value)
                                @if($key =='state')

@if($value != 'Tamil Nadu')
<tr>
                        <td class="text-right" colspan="2">
18.00% IGST
@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@else
<tr>
                        <td class="text-right" colspan="2">9.00% CGST</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
</tr>
                    
<tr>
<td class="text-right" colspan="2">9.00% SGST</td>

@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach

@endif

@endif

@endforeach
</td>
                        


                    </tr>



                    <tr>
                        <td class="text-right" colspan="2">Credit</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'credit')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                        <td class="text-right" colspan="2">Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                                <td><i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    </tbody>
                </table>



</div>
        </div>
    </div>
</div>
@endsection
<!-- /content -->