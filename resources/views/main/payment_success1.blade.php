@extends('layouts.master12')
@section('title')
DeckSys |  Dedicated Servers &  VPS Hosting, Coimbatore India
@endsection
@section('content')
@php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
//print_r($paymentinvoice);exit;
@endphp

<div id="content" class="container" role="main">

    <div class="app-content-body ">
        <div class="bg-light lter b-b wrapper-md hidden-print">
            <a href class="btn btn-sm btn-info pull-right" onClick="window.print(); return false"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a>
   <a href="http://www.maktoinc.com" target="_blank">
<img src="{{asset('img/logo/logo-makto.png')}}"  alt="Makto" class="decksysBrandLogo"></a></div>
        </div>
        <div class="wrapper-md">
            <div>
               
                <div class="row">
 <div class="col-xs-6">
 <a href="http://www.decksys.com" target="_blank">
 <img src="{{asset('img/logo/logo-decksys.png')}}"  alt="Makto" class="decksysBrandLogo" style="margin-top:20px;"></a></div>
                    <div class="col-xs-6 text-right">
                  
			@foreach($paymentinvoice as $key => $invoice)
				@if($key == 'status')
                                	<h1 class="font-initial">{{$invoice}}</h1>
                                @endif
			@endforeach
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : {{$invoice}}</h5>
                            @endif
                            @if($key == 'invoiceid')
                                <h5>Invoice Id : {{$invoice}}</h5>
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            TO:
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                @endif
                            @endforeach
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 text-right">
                            Pay To:
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com<br>
				GST No: 33AAKCM8132C1ZQ<br>
                                CALL : +91 - 73391 88891
                            </p>
                        </div>
                    </div>
                </div>


      <p class="m-t m-b">
                    @foreach($result2['orders']['order']['0'] as $key => $value)
                        @if($key =='date')
                            <p class="m-t m-b">Order date: {{$value}}<br>
                        @endif
                        @if($key =='status')
                            Order status: <span class="label bg-success">{{$value}}</span><br>
                        @endif
                    @endforeach
                    Order ID: {{$order_id}}
               </p>
                <div class="line"></div>
                <table class="table table-striped bg-white b-a">
                    <thead>
                    <tr>
                        <th class="font-initial"  colspan="2">DESCRIPTION</th>
                        <th style="width: 150px" class="font-initial">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
					

                    @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			            <tr>        
                            @for($key = 0; $key < 100; $key++)
                            @endfor
                            <td colspan="2">
                            {{$invoice['description']}} 
                            </td>
                            <td><i class="fa fa-{{$currency_sign}}"></i> {{$invoice['amount']}}</td>
                            @php $var1=$invoice['amount'] @endphp
   			            </tr>
                        

                    @endforeach
                        <tr>
                            <td class="text-right" colspan="2">Sub Total</td>
                            @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                            <td> <i class="fa fa-{{$currency_sign}}"></i> {{$var2=$invoice}}</td>
                            @endif
                            @endforeach

                            
                            
                        </tr>
                        @foreach($clientdetails as $key => $value)
                                @if($key =='state')

@if($value != 'Tamil Nadu')
<tr>
                        <td class="text-right" colspan="2">
18.00% IGST
@foreach($paymentinvoice as $key => $invoice)

                            @if($key == 'tax2')
                            @php $gst= $invoice @endphp
                                <!-- <td> <i class="fa fa-inr"></i> {{$invoice}}</td> -->
                            @endif
                        @endforeach
                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			                 
                             @for($key = 0; $key < 100; $key++)
                             @endfor
                            
                          @if($invoice['type']=='PromoHosting')
                         
                          @php $promogst=-($var1-$var2)*18/100 @endphp
                          
                         
                          <td><i class="fa fa-inr"></i>{{$promogst}}</td> 
                          @else      
                          @php $pro_gst=($var2)*18/100 @endphp
                          <td><i class="fa fa-inr"></i>{{$pro_gst}}</td>                 
                          @endif                         
 
                     @endforeach

@else
<tr>
                        <td class="text-right" colspan="2">9.00% CGST</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                            @php $gst= $invoice @endphp
                                <!-- <td> <i class="fa fa-inr"></i> {{$invoice}}</td> -->
                            @endif
                        @endforeach
                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			                 
                             @for($key = 0; $key < 100; $key++)
                             @endfor
                          @if($invoice['type']=='PromoHosting')
                            @php $promogst1=-($var1-$var2)*9/100 @endphp
                          <td><i class="fa fa-inr"></i>{{$promogst1}}</td>
                          @else 
                          @php $pro_gst=($var2)*9/100 @endphp
                          <td><i class="fa fa-inr"></i>{{$pro_gst}}</td>
                         
                          @endif                         
 
                     @endforeach
                        

                    

<!-- 

                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach -->
</tr>
                    
<tr>
<td class="text-right" colspan="2">9.00% SGST</td>
@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                            @php $gst= $invoice @endphp
                           
                                <!-- <td> <i class="fa fa-inr"></i> {{$invoice}}</td> -->
                            @endif
                        @endforeach
                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			                 
                             @for($key = 0; $key < 100; $key++)
                             @endfor
                          @if($invoice['type']=='PromoHosting')
                          <td><i class="fa fa-inr"></i>{{$promogst1}}</td>
                          @else 
                          @php $pro_gst=($var2)*9/100 @endphp
                          <td><i class="fa fa-inr"></i>{{$pro_gst}}</td>
                          @endif                         
 
                     @endforeach

<!-- @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach -->

@endif

@endif

@endforeach
</td>
                        


                    </tr>



                    <tr>
                        <td class="text-right" colspan="2">Credit</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'credit')
                                <td> <i class="fa fa-inr"></i> {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr> 
                     @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                            @php $tot=$invoice @endphp
                            @endif
                        @endforeach
                        <td class="text-right" colspan="2">Total</td>
                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			                 
                             @for($key = 0; $key < 100; $key++)
                             @endfor
                          @if($invoice['type']=='PromoHosting')
                          <td><i class="fa fa-inr"></i>{{$promogst+$var2}}</td>
                          @else 
                          <td><i class="fa fa-inr"></i>{{$tot}}</td>                
                          @endif                         
 
                     @endforeach
                       
                    </tr>
                    </tbody>
                </table>


                        <div class="line"></div>
<div class="table-responsive">
                        <table class="table table-striped bg-white b-a">
                            <thead>
                            <tr>
                               
                                <th class="font-initial">Transaction Date</th>
                                <th class="font-initial">Gateway</th>
                                <th class="font-initial">Transaction ID</th>
                                <th class="font-initial">Total</th>
                           
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'date')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'gateway')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach

                                    @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'transid')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach
                                    @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			                 
                             @for($key = 0; $key < 100; $key++)
                             @endfor
                          @if($invoice['type']=='PromoHosting')
                          <td><i class="fa fa-inr"></i>{{$promogst+$var2}}</td>
                          @else 
                          <td><i class="fa fa-inr"></i>{{$tot}}</td> 
                                            
                          @endif                         
 
                     @endforeach

                                    <!-- @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'amountin')
                                            <td> <i class="fa fa-{{$currency_sign}}"></i> {{$value}}</td>
                                        @endif
                                    @endforeach -->

                            </tr>
                            <tr><td></td><td></td>
                            <td> Balance </td>
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'amountout')
                                        <td ><i class="fa fa-{{$currency_sign}}"></i> {{$value}}</td>
                                    @endif
                                @endforeach
                            </tr>


                            </tbody>
                        </table>


            </div>
</div>
        </div>
    </div>
</div>

<!-- /content -->
@endsection