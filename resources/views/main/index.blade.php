@extends('layouts.master')
@section('title')
    Decksys
@endsection
@section('content')
@php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
//print_r($products);exit;
@endphp


<section class="dedicated">
	    <div class="container-fluid p-4">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-6 offset-lg-2 col-lg-4 pb-3 pt-4 ">
                     <div class="thumbnail boxShadow">
                        <div class="caption">
                            <h4 class="p-4  text-center">Dedicated Server</h4>
                            <ul class= "list"> 
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> Free SSL security</li>
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> 24x7x365 support team</li>
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> 99.9% SLA1</li>
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> Root access</li>
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> Super fast, high reliability, Solid State Disks</li>
                                <li><i class="fa fa-check colorcheck" aria-hidden="true"></i> Global Data Centers</li>   
                            </ul>
                            <div class="wrapper">
                            @foreach($products['products']['product'] as $value)
									@if($value['gid'] == "6") 
										@if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
											@foreach($value['pricing'] as $cur => $key)      
											  @if($cur==$currency)
											   <h3 class="sub p-3"><i class="fa fa-{{$currency_sign}}" aria-hidden="true"></i><strong>{{$key['monthly']}}</strong></h3>
											@endif
									@endforeach
								@endif
							
						@endif
					@endforeach	
                                
                                <a href="DedicatedServer" class="btn btn-primary btn-capsul px-4 py-2 "> Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-6 col-lg-4 pt-4 ">
                    <div class="thumbnail boxShadow1">
                        <div class="caption">
                            <h4 class="p-4   text-center">Affordable VPS</h4>
                            <ul class= "list"> 
                                <li><i class="fa fa-check" aria-hidden="true"></i> Free SSL security</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i> 24x7x365 support team</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i> 99.9% SLA1</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i> Root access</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i> Super fast, high reliability, Solid State Disks</li>
                                <li><i class="fa fa-check" aria-hidden="true"></i> Global Data Centers</li>    
                            </ul>
                            <div class="wrapper">
                            @foreach($products['products']['product'] as $value)
									@if($value['gid'] == "16") 
										@if($value['name'] == "VPS 0.5GB")
											@foreach($value['pricing'] as $cur => $key)      
											  @if($cur==$currency)
											<h3 class="sub p-3 text-white"><i class="fa fa-{{$currency_sign}}" aria-hidden="true"></i><strong>{{$key['monthly']}}</strong></h3>
											@endif
									@endforeach
								@endif
							
						@endif
					@endforeach	
                                <a href="VPS" class="btn btn-primary btn-capsul px-4 py-2 "> Buy Now</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- End row -->
        </div><!-- End container -->
	</section>
	
    <section class="featureSection">
        <div class="container-fluid">
            <div class="row">
                
                    <div class=" col-12 offset-sm-1 col-sm-3 col-md-2 col-lg-2 col-xl-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="phoneSupport"></div>
                            <div class="featureContentContainer">
                                <h4>24 x 7 Support</h4>
                                <p>Talk With Our Experts</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="liveChat"></div>
                            <div class="featureContentContainer">
                                <h4>Live Chat</h4>
                                <p>Get Instant Update</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-3 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="upTime"></div>
                            <div class="featureContentContainer">
                                <h4>99.9% Uptime</h4>
                                <p>It's a Pinky Promise</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="submitTicket"></div>
                            <div class="featureContentContainer">
                                <h4>Submit a Ticket</h4>
                                <p>We Solve Your Hosting Issues</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-12 col-sm-6 col-md-2 col-lg-2">
                        <div class="featureContainer">
                            <div class="featureImageContainer" id="moneyBack">
                                
                            </div>
                            <div class="featureContentContainer">
                                <h4>30 Days Money back</h4>
                                <p>Gaurantee</p>
                            </div>
                        </div>
                    </div>
                    <div class="offset-sm-1"></div>
                </div>
            </div>
        </div>
    </section>
	<section class="FAQ">
	    <h2 class="text-center p-4 "><b>FAQ</b></h2>
        <div class="container-fluid">
            <div class="col-md p-4">
                <center>
                    <div class="col-md-8">
                        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingOne">
                                    <h4 class="panel-title">
                                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                           1. Is there a root access for my server?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse show" role="tabpanel" aria-labelledby="headingOne">
                                    <div class="panel-body">
                                        <p>Yes, every server comes with a root access and have a complete control over both the VPS and Dedicated.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingTwo">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                            2. When will be server be available for use?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo">
                                    <div class="panel-body">
                                        <p>Your servers are available immediately once you place the order and the payment processed successfully. We have an automated provisioning system to handle the operation.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingThree">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree">
                                            3. What is the type of support do I get?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree">
                                    <div class="panel-body">
                                        <p>We offer you Managed support for both dedicated and VPS servers. Refer our <a class="text" href="ManagedServices.html">�Managed Services�</a> section
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFour">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFour" aria-expanded="false" aria-controls="collapseFour">
                                            4. What are the softwares that I can install?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour">
                                    <div class="panel-body">
                                        <p>You can install any softwares that are legal to use, except those which are malware and which contains viruses. The servers are behind a secured firewall and any detection of such software will result in termination.
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <div class="panel panel-default">
                                <div class="panel-heading" role="tab" id="headingFive">
                                    <h4 class="panel-title">
                                        <a class="collapsed" role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseFive" aria-expanded="false" aria-controls="collapseFive">
                                            5. How can I connect to the server?
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                                    <div class="panel-body">
                                        <p>You can use software PuTTY and also SSH to the server. In case of Windows Dedicated Server, you will be provided single access to RDP port.
                                        </p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </center>
            </div>
        </div>
    </section>
	<!-- <section class="home-newsletter" style="background-image: url(../resources/img/Home/dedicated_server1.jpg)">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div class="single">
                        <h2>Subscription</h2>
                        <div class="input-group">
                            <input type="email" class="form-control" placeholder="Enter your email">
                            <span class="input-group-btn">
                                <button class="btn btn-theme" type="submit">Submit</button>
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section> -->
    @endsection