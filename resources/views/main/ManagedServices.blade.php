@extends('layouts.master')

@section('title')
DeckSys |  Dedicated Servers &  VPS Hosting, Coimbatore India
@endsection
@section('content')

<section class="vpsServer">
<div class="container-fluid">
    <div class="row py-5">
            <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12">
                 <h2 class="text-center pb-4 "><b>
                    VPS SERVER MONITORING SERVICES WITH <strong style="color:#29539e;">PROACTIVE SUPPORT</strong>
                </b>
                </h2>
                <p class="card text-center p-4">
                    At decksys.com we offer two types of managed services solutions:
                    Server Monitoring and Monitoring + Configuration. These services are ideal for customers who wish to maintain a hands-off approach for their web hosting services.
                    For example, if you would you like us to help you configure your server, simply let us know the requirements and we’ll take over for you. Our friendly and helpful in-house expertise is available 24/7/365.
                </p>
            </div>
            <div class="container" style="display:none;">
            
            
                <table class="table-bordered" width="100%"  >
              <thead>
                <tr>
                  <th class="pro"></th>
                  <th class="prod">Server Monitoring</th>
                  <th class="prod">Monitoring & Configuration</th>
                </tr>
              </thead>
              <tbody>
                <tr>
                  <td class="features">Monitoring <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle"><img   src="/img/ico/competitors-yes.png"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                </tr>
                <tr>
                  <td class="features">Proactive Support <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                </tr>
                <tr>
                  <td class="features">Response SLA <img src="/img/ico/info-small.png" class="info"></td>
                  <td align="center" valign="middle">30 Minute</td>
                  <td align="center" valign="middle">15 Minute</td>
                </tr>
                  <tr>
                    <td class="features">Backup Setup <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Assisted Start-up <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Server Configuration <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Site Migration <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Server Security <img src="/img/ico/info-small.png" class="info"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-no.png"></td>
                    <td align="center" valign="middle"><img src="/img/ico/competitors-yes.png"></td>
                  </tr>
                  <tr>
                    <td class="features">Price</td>
                    <td align="center" valign="middle">$15.00/mo</td>
                    <td align="center" valign="middle">$30.00/mo</td>
                  </tr>
              </tbody>
            </table>
    </div>
</div>
</section>

<section class="supportFeatures">
<div class="container-fluid">
    <h2 class="text-center py-4"> <strong style="color:#29539e;">OUR MANAGED SERVICES</strong> </h2><hr>
    <p class="text-center px-5 py-1">Our support team has the expertise to help with any of the areas listed below. Can’t find your issue or item on the list? <a class="text" href="">Contact us here</a> and we will assist you with all of your questions.</p>
</div>
<div class="container p-5">
    <div class="row ml-4 border-bottom border-dark">
    <div class="col-12 col-sm-6 col-md-4 col-lg-4 border-right border-dark">
            <h5 class="managedServices-heading"><strong>Application Services</strong></h5>
            <ul >
                <li>Application deployment</li>
                <li>Mail Server (Dovecot, Postfix, Sendmail)</li>
                <li>Database Server and Management ( MySQL, MsSQL Express)</li>
                <li>webmail Configuration (Horde/Squirrelmail/Roundcube)</li>
                <li>OS Installation & More</li>
              
            </ul>

        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 border-right border-dark">
            <h5 class="managedServices-heading"><strong>Server Management and Security</strong></h5>
            <ul >
                <li>Firewall Management ( CSF + LFD + APF,PFSense</li>
                <li>Antivirus</li>
                <li>SSL Certificate Management & Installation & more</li>
                

            </ul>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4">
            <h5 class="managedServices-heading"><strong>Networking & Protocols</strong></h5>
            <ul >
                <li>FTP, SFTP / SSH, HTTP / HTTPS, POP & IMAP ,SMTP</li>
                <li>DNS Zone Management</li>
                <li>Spam Filtering / IP Config & more</li>
                

            </ul>
        </div>
        
    </div>
    <div class="row ml-4">     
    <div class="col-12 col-sm-6 col-md-4 col-lg-4 border-right border-dark p-3">
            <h5 class="managedServices-heading"><strong>Management Software</strong></h5>
            <ul >
                <li>Cpanel</li>
                <li>Plesk</li>
            </ul>
        </div>
        <div class="col-6 col-sm-6 col-md-4 col-lg-4 border-right border-dark p-3">
            <h5 class="managedServices-heading"><strong>Server Monitoring</strong></h5>
            <ul >
                <li>Backup</li>
                <li>Site Migration</li>
            </ul>
        </div>
       
        
    </div>
</div>
</section>

<section class="call-now" >
    <div class="container-fluid">
         <h2 class="text-center h2-mt py-4">NEXT STEPS</h2>
         <p class="text-center lines pb-4">
        New customers can order their preferred Managed Services & Support package during the ordering process on our
         <a class="text" href="/VPS">SSD VPS</a> or
         <a class="text" href="/DedicatedServer">Dedicated Srever</a> product pages. Existing customers simply need to
         <a class="text" href="/img/ico/login.png">log into their account</a> to order new Managed Services & Support. For further information, call
         +91 84484 44086
          or <a class="text" href="#Comm100API.open_chat_window(event, 1529);">Chat Live</a> with our sales team.
            </p>
        <div class="container text-center">
        <div class="row py-4">
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">LOG IN TO ORDER</h5>
          <a href="#"class="btn btn-primary btn-capsul  px-4 py-2"><img src="/img/ico/login-icon.png" class="call-now-icon-helper "> LOG IN TO ACCOUNT</a>
        </div>
  
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">FOR FURTHER INFORMATION</h5>
            <a class="text" href="+91 84484 44086" ><img src="/img/ico/telephone-icon.png" > CALL <strong>NOW ON</strong> +91 84484 44086</a>
        </div>
        <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3 br">
          <h5 class="pb-2">CHAT LIVE TO SALES TEAM</h5>
          <a href="#" class="btn btn-primary btn-capsul  px-4 py-2"><img src="/img/ico/chat-icon.png" > LIVE CHAT</a>
        </div>
      </div>
    </div>
    </div>
  </section>	
@endsection
