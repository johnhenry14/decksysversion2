@extends('layouts.mastercart')
@section('title')
    Decksys
@endsection

@section('content')

<section class="supportSection1 pt-4 pb-4" >
		<div class="text-center p-3">
			<h2><b>SUPPORT FROM DECKSYS	</b></h2>
		</div>
		<div class="container-fluid">
			<div class="row text-center">
                <div class="col-12 col-sm-12 col-md-6  col-lg-4 pb-3">
					<div class="card cards pb-5">
						<div class="card-body">
							<h4 class="card-title"><strong>SEARCH OUR <br>KNOWLEDGEBASE</strong></h4>
							<p class="card-text">Hundreds of articles written with the purpose of answering your questions.</p>
							<a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-search" aria-hidden="true"></i> SEARCH NOW</a>
						</div>
					</div>
				</div>
                <div class="col-12 col-sm-12 col-md-6 col-lg-4 pb-3">
					<div class="card cards pb-5" >
						<div class="card-body">
							<h4 class="card-title"><strong>EMAIL A DECKSYS <br> DEPARTMENT</strong></h4>
							<p class="card-text">Get the help you need by sending an email through our simple online form.</p>
							<a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-envelope" aria-hidden="true"></i> SUBMIT A REQUEST</a>
						</div>
					</div>
				</div>
                <div class="col-12 col-sm-12 col-md-12 col-lg-4">
					<div class="card cards pb-5">
						<div class="card-body">
							<h4 class="card-title"><strong>CHAT WITH A <br> SUPPORT AGENT</strong></h4>
							<p class="card-text">Our trained staff is online 24/7 ready to assist you in a chat session.</p>
							<a href="#" class="cardButton btn btn-lg btn-success"><i class="fa fa-comments-o" aria-hidden="true"></i> START A CHAT</a>
						</div>
					</div>
				</div>
			</div>
		</div>
    </section>
    
    <section class="contact">
        <div class="container-fluid">
            <div class="row">
               
       
               <div class="col-md-6 col-sm-6 col-xs-12 contactAddress">
               
                       <div class="panel panel-success">
                           <div class="text-center mapHeader"><h2>Reach Us</h2>
                           <hr class="size">
                           </div>
                           <div class="panel-body">
                             
                               <p class="text-justify">
       
       Makto Technology Private Limited,
       <br> Block B2 1st Floor,<br>Rathinam Technical Campus,<br>Pollachi Main Road,Eachanari,<br>Coimbatore 641 021<br><br>
       <b>Sales</b><br>
                               Email -  <a href="mailto:sales@decksys.com" style="color:#51a3db">sales@decksys.com</a> <br>
       Phone Number - (91) 40 4596 7079 (Press 1 for Sales)<br><br>
       <b>Support</b><br> 
                              Email -  <a href="mailto:support@decksys.com" style="color:#51a3db">support@decksys.com</a><br>
       
                               Phone Number - (91) 40 4596 7079 (Press 2 for Support)
       
       </p>                        
                               
                         
                           </div>
                       </div>
                   </div>
                   <div class="col-md-6 col-sm-6 col-xs-12 p-3">
               
                    <div class="mapouter"><div class="gmap_canvas">
                        <iframe width="640" height="500" id="gmap_canvas" src="https://maps.google.com/maps?q=Block%20B2%2C%201st%20Floor%2C%20Rathinam%20Technical%20Campus%2C%20Pollachi%20Main%20Road%2C%20Eachanari%2C%20Coimbatore%20641021&t=&z=15&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
                        </div><style>.mapouter{height:500px;width:650px;}.gmap_canvas {background:none!important;height:500px;width:600px;}</style>
                        </div>
                        
                   </div>
               </div>
       </div>
       
    </section>
	
	
@endsection