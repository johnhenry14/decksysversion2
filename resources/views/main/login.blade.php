@extends('layouts.mastercart')
@section('title')
    Decksys | Signin
@endsection
@section('content')

        <div class="container" style="max-width: 100% !important;">
        <div class="row pb-5" style="background-color: #fff;">
            <div class="offset-lg-2 col-lg-8 col-sm-12 p-5 border rounded main-section"style="margin-top:55px; background-color: ghostwhite;">
              
                <?php if(isset($_GET['error'])){ ?>
                <p style="text-align: center;color: red;font-size: 18px;">Enter valid Email or Password</p>
                <?php } ?>
                <?php if(isset($_GET['success'])){ ?>
               <p style="text-align: center;color: #fff;font-size: 18px;">Password Changed Successfully</p>
                <?php } ?>
                <h3 class="text-center p-3" style="color:steelblue">Signin</h3>
                <form class="container" action="{{route('login.signin_action')}}" method="post" id="needs-validation" novalidate>
 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                    <div class="row">
                        <div class="offset-lg-2 col-lg-8 col-sm-12 col-12" >
                            <div class="form-group">
                                <label class="text-inverse" for="validationCustom01">Email<span class="req">*</span></label>
                                <input type="Email" class="form-control" name="email" id="validationCustom01" placeholder="Email" value=""maxlength="100">
                                <div class="invalid-feedback">
                                    Enter Your Email
                                </div>
                            </div>
                        </div></div>  <div class="row">
                        <div class="offset-lg-2 col-lg-8 col-sm-12 col-12" >
                            <div class="form-group">
                                <label class="text-inverse" for="validationCustom02">Password<span class="req">*</span></label>
                                <input type="password" class="form-control" name="password2" id="validationCustom02" placeholder="Password" value=""maxlength="25">
                                <div class="invalid-feedback">
                                    Enter Your Password
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="row">

                        <div class="col-lg-12 col-sm-12 col-12">
                            <div class="form-group">


                                <p  class="text-center text-dark"><a class="text-dark" href="/forgotpassword" >Forgot Password</a> |<a class="text-dark" href="/Register"> Create an Account</a></p>

                            </div>
                        </div>
                    </div>
 <div class="row">
                        <div class="col-lg-12 col-sm-12 col-12">
                            <div class="form-group">
                                <p class="text-center"><button class="btn btn-info" type="submit">Login</button> </p>

                            </div>
                        </div>
                    </div>
                    
                </form>
		
            </div>
        </div>
    </div>

@endsection