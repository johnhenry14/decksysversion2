@extends('layouts.mastercart')
@section('title')
    DeckSys | Dedicated Servers, VPS, Coimbatore India
@endsection
@section('content')
@php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
//echo "<pre>";print_r($cartdata);exit;
@endphp

<section class="tabContentContainer">

        <div class="container">
                    <div class="row">
                <div class="col-xs-12 col-sm-12 col-md-10 col-lg-12" style="margin-top:40px;">
                    <section class="tabContent active" id="linuxHosting">
                     <div class="table-responsive">
					 @if (Session::has('message'))
        <p style="text-align: center;color: green;font-size: 18px;">Promotion Code Accepted! Your order total has been updated.</p>
	
        @endif
		@if (Session::has('message_error'))
        <p style="text-align: center;color: red;font-size: 18px;">The promotion code entered does not exist</p>
        @endif
		@if (Session::has('message_error1'))
        <p style="text-align: center;color: red;font-size: 18px;">This promotion code already applied.</p>
        @endif
        @if (Session::has('message_error2'))
        <p style="text-align: center;color: red;font-size: 18px;">This promotion code is not applicable to this package.</p>
        @endif
        @if (Session::has('message_promo'))
        <p style="text-align: center;color: blue;font-size: 18px;">Promotion Code Applied Successfully</p>
        @endif
        @if (Session::has('message_delete'))
        <p style="text-align: center;color:red;font-size: 18px;">Order Deleted Successfully</p>
        @endif
        @if (Session::has('message_empty'))
        <p style="text-align: center;color:red;font-size: 18px;">Your Cart is Empty, Add atleast one ! </p>
        @endif
      
                        <table class="table">
                            <thead>
                            <tr>
                            <td>S.No</td>
                                <td colspan="1">Order Details</td>
                                <td>Cost</td>
                                <td>Actions</td>
                            </tr>
                            </thead>
                            <tbody> 
                             <?php 
                             $tax_total=0;
                             $sub_total=0;$sub_total1=0;
                             $discount_price=0;
                             $discount_code='';
                             $discount_percentage=0;
                             $discount_amount=0;
                             $count=Cart::count();
                             ?>
                             <?php $i=0; ?>
                            @foreach($cartdata as $key=>$value)    
                            <?php $i++; ?>                 
                                
								
								 
                                     <tr>
                                    <td>{{$i}}  <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="rowid" value="{{$value->rowId}}" />
                                   </td>

                                    <td>
                                        @if($value->options->package == 'Dedicated Server')
                                        <b>{{$value->options->description}}</b><br>
                                        <b>1. Operating System - </b> {{$value->options->os}}<br>
                                        <b>2. Database - </b>{{$value->options->db}} 
                                        @endif
                                        @if($value->options->package == 'VPS Server')
                                        <b>{{$value->options->description}}</b><br>
                                        <b>1. Operating System - </b>{{$value->options->os}}<br>
                                        <b>2. Database - </b>{{$value->options->db}}<br>
                                        <b>3. Service Type - </b>{{$value->options->st}}<br>
                                        <b>4. DataBackup - </b>{{$value->options->dp}}<br>
                                        <b>5. Datacenter - </b>{{$value->options->dc}}<br>
                                        <b>6. Control panel - </b>{{$value->options->cp}}<br>
                                                                           
                                        @endif
                                    </td>                                                                                                                                                                 
                                    <td style="padding-top: 15px;">
                                    <i class="fa fa-{{$currency_sign}}"></i>
                                    
                                      {{round($value->options->price1,2)}}  </td>
                                    <td>
                                    <form action="{{route('cart.remove')}}" method="POST">
                                    <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
                                    <input type="hidden" name="rowid" value="{{$value->rowId}}" />
                                    <button type="submit" class="btn btn-danger center-block" style="border-radius: unset;"><i class="fa fa-trash" aria-hidden="true"></i></button> 
                                    </form>                                                                        
                                    </td>
                                    </tr>
                                    
                                    <?php 
                                    $sub_total1=$value->options->price1;
                                    if($value->options->discount_code!=''){ 
                                        $discount_code=$value->options->discount_code;
                                       $discount_percentage=$value->options->discount_percentage;
                                    } 
                                        ?>
                                    
                                
							        <?php  $tax_total=$tax_total+$value->tax; $sub_total=$sub_total+$value->subtotal;?>
                                    @endforeach
                         
                         
                            </tbody>
                            <tfoot>
                            <?php if($discount_code!=''){ ?>
                            <tr>                                
                                        <td colspan="2" class="text-right"> Discount ( {{ $discount_code }}, {{ $discount_percentage }} %)</td>                             
                                        <td style="padding-top: 15px;">
                                        <i class="fa fa-{{$currency_sign}}"></i> 
                                       
                                        {{$discount_price=round(($sub_total1*$discount_percentage)/100,2)}} </td>
                                        <td></td>
                                    </tr> 
                                    <tr>                                
                                        <td colspan="2" class="text-right"> Sub Total</td>                             
                                        <td style="padding-top: 15px;"> 
                                        <i class="fa fa-{{$currency_sign}}"></i>
                                         {{$sub_total}}  </td>
                                        <td></td>
                                    </tr> 
                            <?php } ?>
                            @if($currency=="INR")
                             <tr>                                
                                <td colspan="2" class="text-right"> GST @ 18.00%</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{round($g=$tax_total,2)}}</td>
                                <td></td>
                            </tr> 
                            @else
                           
                            @endif
                            @if($currency=="INR")
                            <tr>                                
                                <td colspan="2" class="text-right"> Total</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{$g+$sub_total}}</td>
                                <td></td>
                            </tr>
                            @else
                            <tr>                                
                                <td colspan="2" class="text-right"> Total</td>                             
                                <td style="padding-top: 15px;"> 
                                <i class="fa fa-{{$currency_sign}}"></i>
                                {{round($sub_total,2)}}</td>
                                <td></td>
                            </tr>
                            @endif

                       
                     
                            <!-- <tr >
                            <td colspan="3" style="border:none"><a href="/" class="btn" style="background-color: #7266ba; color:#FFF"><i class="fa fa-angle-left"></i> Continue Shopping</a></td>
                            
                            <td style="border:none"><a href="/checkout" class="btn btn-success btn-block">Checkout <i class="fa fa-angle-right"></i></a></td>
                            </tr> -->
							<tr>
							<td colspan="6">

                            
							<div class="form-group">
							
							<form action="{{route('cart.apply_promo')}}" method="post">
									 <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">

                                     
                                     <div class="input-group mb-3 offset-lg-2 col-lg-8">
                                     <input type="text" id="newLayerName" name="code" class="form-control field" placeholder="Enter promo code if you have one" required="required" style="width: 59%;"
                                      <?php if( $discount_code!='' || $count=='0')
                                       {
                                           ?> value="{{$discount_code}}"  disabled 
                                           <?php } ?>/>
  <div class="input-group-append">
    <span class="input-group-text" id="basic-addon2"><a href="{{route('cart.delete_promo')}}" style="color:red; text-decoration:none">Remove Promotion Code</a></span>
  </div>
</div>
       
							</div>
                            



							<div class="form-group">
                            <button type="submit" id="btnSaveLayer" class="btn btn-info center-block" onclick="SaveNewLayer()"
                              disabled>Validate Code</button>
							</div>

                            </form>
                            
                            <?php
                            

                            if($count=='0')
{
    echo '<a href="javascript:void(0);" style="display:none;">Your Text For Link</a>';
} 
else
{
    echo ' <a href="/checkout" class="btn btn-success text-left" style="margin-left: 43%;">Order Now <i class="fa fa-angle-right"></i></a>';
}
                            ?>
							</td>
							</tr>
                            
                            </tfoot>
                        </table>
					</div>
                    </section>
                </div>
            </div>
        </div>
    </section>
    <script>
function setElementsDisabled() {
  $('#newLayerName').keyup(function() {
      $('#btnSaveLayer').prop('disabled', $(this).val().length == 0);
  })
}
setElementsDisabled()

$(".clear").click(function() {
  $("#newLayerName").val('').trigger("keyup");
})
</script>
	
@endsection