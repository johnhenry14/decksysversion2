@extends('layouts.mastercart')
<!-- content -->

@section('title')
DeckSys |  Dedicated Servers &  VPS Hosting, Coimbatore India
@endsection
@section('content')
<?php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
$country=session()->get('country');
?>
	<!-- <div class="loader"></div> -->
<div class="container" role="main">
<div class="row pt-3">
<div class="col-lg-2 offset-lg-10"><a href class="btn btn-sm btn-info" id="printPageButton" onClick="window.print(); return false"><i class="fa fa-print" aria-hidden="true"></i>
                &nbsp; Print</a></div></div>

       </div>

       <div class="container">
   
               
                <div class="row">
                
 <div class="col-lg-2">
 <a href="/">
 <img src="{{asset('img/logo/logo-decksys.png')}}"  alt="decksys" class="decksysBrandLogo" style="margin-top:20px;"></a></div>
                    <div class="col-lg-4 offset-6 text-right">
                    <h2 class="text-success">Proforma Invoice</h2>
			@foreach($paymentinvoice as $key => $invoice)
				@if($key == 'status')
                                	<h1 class="font-initial">{{$invoice}}</h1>
                                @endif
			@endforeach
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'date')
                                <h5>Invoice Date : {{$invoice}}</h5>
                            @endif
                            @if($key == 'invoiceid')
                                <h5>Invoice Id : {{$invoice}}</h5>
                            @endif

                        @endforeach
                    </div>
                </div>
                <div class="well m-t bg-light lt p-4">
                    <div class="row">
                        <div class="col-xs-6 col-sm-6">
                            TO:
                            @foreach($clientdetails as $key => $value)
                                @if($key == 'fullname')
                                    <h4>{{$value}}</h4>
                                @elseif($key == 'companyname')
                                    {{$value}}<br>
                                @elseif($key == 'phonenumber')
                                    Phone: {{$value}}<br>
                                @elseif($key == 'address1')
                                    {{$value}}<br>
                                @elseif($key == 'city')
                                    {{$value}}<br>
                                @elseif($key == 'state')
                                    {{$value}}<br>
                                @elseif($key == 'postcode')
                                    {{$value}}<br>
                                @elseif($key == 'email')
                                    Email: {{$value}}<br>
                                @endif
                            @endforeach
                            
                        </div>
                        <div class="col-xs-6 col-sm-6 text-right">
                            Pay To:
                            <h4>Makto Technology Pvt Ltd</h4>
                            <p>B2, First Floor, Rathinam Technical Campus<br>
                                Eachanari, Pollachi Main Road,<br>
                                Coimbatore 641 021, Tamilnadu, India.<br>
                                Email: info@maktoinc.com<br>
				GST No: 33AAKCM8132C1ZQ<br>
                                CALL : +91 - 73391 88891
                            </p>
                        </div>
                    </div>
                </div>


      <p class="m-t m-b">
                    @foreach($result2['orders']['order']['0'] as $key => $value)
                        @if($key =='date')
                        <p class="m-t m-b pl-3">Order date: {{$value}}<br>
                            @endif
                        @if($key =='status')
                    Order status: <span class="label bg-success p-1">{{$value}}</span><br>
                        @endif
                    @endforeach
                    Order ID: {{$order_id}}
               </p>
                <div class="line"></div>
                <table class="table table-striped bg-white b-a">
                    <thead>
                    <tr>
                        <th class="font-initial"  colspan="2">DESCRIPTION</th>
                        <th style="width: 150px" class="font-initial">TOTAL</th>
                    </tr>
                    </thead>
                    <tbody>
					

                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			<tr>
			                @if($key%2!=0)				
							    @for($key = 0; $key < 100; $key++)
							    @endfor
  		

 <td colspan="2">
{{$invoice['description']}} 

</td>
<td>
 @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach 


{{$invoice['amount']}}</td>
@endif
   			</tr>

                        @endforeach

                        @foreach($paymentinvoice['items']['item'] as $key => $invoice)
			<tr>
			@if($key%2==0)				
							@for($key = 0; $key < 100; $key++)
							@endfor
  		

 <td colspan="2">
{{$invoice['description']}} 

</td>
<td>@foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach  {{$invoice['amount']}}</td>
@endif
   			</tr>

                        @endforeach
                
                    <tr>
                        <td class="text-right" colspan="2">Sub Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td> @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach  {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    

                    
@foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN')

  @if($key =='state')
@if($value != 'Tamil Nadu')
                    <tr>
                        <td class="text-right" colspan="2">18.00% IGST</td>
                            @foreach($paymentinvoice as $key => $invoice)
                                @if($key == 'tax2')
                                    <td> @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach  {{$invoice}}</td>
                                @endif
                            @endforeach</tr>

                                @else
                    <tr>
                        <td class="text-right" colspan="2">9.00% CGST</td>
                            @foreach($paymentinvoice as $key => $invoice)
                                @if($key == 'tax')
                                <td>  @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                                @endif
                            @endforeach
                    </tr>
                    
<tr>
<td class="text-right" colspan="2">9.00% SGST</td>

@foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'tax2')
                                <td>  @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
@endif


@endif
@else

@endif
@endif
@endforeach
</td>
                        


                    </tr>



                    <tr>
                        <td class="text-right" colspan="2">Credit</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'credit')
                                <td>  @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
                    </tr>
                    <tr>
                    @foreach($clientdetails as $key => $value)
@if($key=='countryname')
@if($value == 'India')
                        <td class="text-right" colspan="2">Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                                <td> @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
                        @else
                        <td class="text-right" colspan="2">Total</td>
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td>@foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
                        @endif
                        @endif
                        @endforeach

                    </tr>
                    </tbody>
                </table>


                        <div class="line"></div>
<div class="table-responsive">
                        <table class="table table-striped bg-white b-a">
                            <thead>
                            <tr>
                               
                                <th class="font-initial">Transaction Date</th>
                                <th class="font-initial">Gateway</th>
                                <th class="font-initial">Transaction ID</th>
                                <th class="font-initial">Total</th>
                           
                            </tr>

                            </thead>
                            <tbody>
                            <tr>
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                    @if($key == 'date')
                                        <td>{{$value}}</td>
                                    @endif
                                @endforeach
                                @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'gateway')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach

                                    @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'transid')
                                            <td>{{$value}}</td>
                                        @endif
                                    @endforeach
                                 
                    @foreach($clientdetails as $key => $value)
@if($key=='countryname')
@if($value == 'India')
                        
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'total')
                                <td> @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
                        @else
                 
                        @foreach($paymentinvoice as $key => $invoice)
                            @if($key == 'subtotal')
                                <td> @foreach($clientdetails as $key => $value)
@if($key=='country')
@if($value == 'IN') 
<i class="fa fa-inr"></i>
@else
<i class="fa fa-usd"></i>
@endif
@endif
@endforeach {{$invoice}}</td>
                            @endif
                        @endforeach
                        @endif
                        @endif
                        @endforeach

                   
                                    <!-- @foreach($paymentinvoice['transactions']['transaction']['0'] as $key => $value)
                                        @if($key == 'amountin')
                                            <td> <i class="fa fa-{{$currency_sign}}"></i> {{$value}}</td>
                                        @endif
                                    @endforeach -->

                            </tr>
                            <tr>
                            <td colspan="3" class="text-right"> Balance </td>
                            
                                @foreach($paymentinvoice as $key => $invoice)
                                    @if($key == 'balance')
                                        <td>@foreach($clientdetails as $key => $value)
                                            @if($key=='country')
                                            @if($value == 'IN') 
                                            <i class="fa fa-inr"></i>
                                            @else
                                            <i class="fa fa-usd"></i>
                                            @endif
                                            @endif
                                            @endforeach {{$invoice}}</td>
                                    @endif
                                @endforeach
                            </tr>


                            </tbody>
                        </table>


            </div>
</div>
        </div>
    </div>
</div>
</div>

<!-- /content -->
@endsection