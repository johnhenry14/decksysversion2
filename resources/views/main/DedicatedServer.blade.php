@extends('layouts.master')

@section('title')
   Decksys | Dedicated Server
@endsection
@section('content')

@php 
$currency=session()->get('currency');
$currency_sign=session()->get('currency_sign');
$country=session()->get('country');
//print_r($country);exit;
@endphp


<div class="container-fluid  text-center mb-0">
	<div class="row dedicatedServer">
	 <div class="col-xl-12 col-lg-10 col-md-12 col-sm-12 col-12">
			<h2>BENEFITS WITH OUR DEDICATED SERVER :<strong style="color:#29539e;"> Affordable, Fast,
			<br> Secure, Complete </strong></h2>
	 	</div>
		
		<div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 col-12 dedicatedServer"> 
	 		<div class="table-responsive">	 
			 
				<table class="table">
		  	<thead>
				<tr>
					<th>PROCESSOR</th>
					<th>RAM</th>
					<th>BANDWIDTH/Mo</th>
					<th>STORAGE</th>
					<th>IP ADDRESS</th>
					<th>OPERATING SYSTEM</th>
					<th>DATABASE</th>
					<th>PRICE</th>
					<th>ORDER</th>
				</tr>
		  	</thead>
		  	<tbody>
					<tr>
					<form name="myform" id="myform"  action="{{route('cart.add_dedicated_server')}}" >  		
						<td>Xeon E3 1230v3 Quad Core Server</td>
						<td>8GB</td>
						<td>5TB</td>
						<td>1TB SATA/120 GB SSD</td>
						<td>1 IP</td>		
						
			  				@foreach($products['products']['product'] as $value)
									@if($value['gid'] == "6") 
										@if($value['name'] == "Xeon E3 1230v3 Quad Core Server")
											@foreach($value['pricing'] as $cur => $key)      
											  @if($cur==$currency)
											
											<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
															<input type="hidden" name="name1" value="{{$value['name']}}" />
															<input type="hidden" name="msetupfee" value="{{$key['msetupfee']}}" />
															<input type="hidden" name="gid" value="{{$value['pid']}}" />
															<input type="hidden" name="description" value="{{$value['description']}}" />
															
												@foreach($value['configoptions']['configoption'] as $configure)                                  
													@if($configure['name']=='OS')
													
														<td>
															<div class="form-group">
														
																<select class="form-control price_change1" name="os" id="txt1"  data-field="txt4" data-Pfield="txt2" data-Ffield="txt3" required>	
																	<option value="">--select--</option>										
																		@foreach($configure['options']['option'] as $options)   
																			<option value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
																		@endforeach
																</select>

																
															</div>									
													</td>
												@elseif($configure['name']=='Database')
													<td>									
													<div class="form-group">
														<select class="form-control price_change target" name="db" id="txt2"  data-field="txt4" data-Pfield="txt1" data-Ffield="txt3">
													
																@foreach($configure['options']['option'] as $options)   
																	<option name="price1" value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
																@endforeach
														</select>
													</div>
												</td>
											@endif
										@endforeach						
												<td>
													<input class="form-control" id="txt3" type='hidden' value="{{$key['monthly']}}" />

											<input class="form-control" id="txt4" type='text' name="price" value="<?php if($currency=="INR") { echo "₹";	}	else { echo "$";} ?>{{$key['monthly']}}" readonly/>
	
												</td>
												@endif
									@endforeach
								@endif
							</div>
						@endif
					@endforeach		
						    <td class="text-center"><button type="submit" class="btn btn-md btn-success">ORDER NOW</button></td>
					 </form>
				</tr>

			<!-- Xeon E3 1240v5 Quad Core Server -->
			<tr>
			<form name="myform" id="myform"  action="{{route('cart.add_dedicated_server')}}" >  
			  <td>Xeon E3 1240v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>
				@foreach($products['products']['product'] as $value)
					@if($value['gid'] == "6")  
						@if($value['name'] == "Xeon E3 1240v5 Quad Core Server")
							@foreach($value['pricing'] as $cur => $key)      
							@if($cur==$currency)
							<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
															<input type="hidden" name="name1" value="{{$value['name']}}" />
															<input type="hidden" name="msetupfee" value="{{$key['msetupfee']}}" />
															<input type="hidden" name="price" value="{{$key['monthly']}}" />
															<input type="hidden" name="gid" value="{{$value['pid']}}" />
									                        <input type="hidden" name="description" value="{{$value['description']}}" />
								@foreach($value['configoptions']['configoption'] as $configure)                                  
									@if($configure['name']=='OS')
										<td>
											<div class="form-group">
												<select class="form-control price_change1" name="os" id="txt5" data-field="txt8" data-Pfield="txt6" data-Ffield="txt7" required>	
												<option value="">--select--</option>										
												@foreach($configure['options']['option'] as $options)   
													<option value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
												@endforeach
												</select>
											</div>									
										</td>
									@elseif($configure['name']=='Database')
									<td>									
										<div class="form-group">
											<select class="form-control price_change target"  name="db" id="txt6" data-field="txt8" data-Pfield="txt5" data-Ffield="txt7">
												
													@foreach($configure['options']['option'] as $options)   
														<option value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
													@endforeach
											</select>
										</div>
									</td>
									@endif
								@endforeach						
								<td>
									<input class="form-control" id="txt7" type='hidden' value="{{$key['monthly']}}" />
									<input class="form-control" id="txt8" type='text' name="price" value="<?php if($currency=="INR") { echo "₹";	}	else { echo "$";} ?>{{$key['monthly']}}" readonly/>
								</td>
								@endif
							@endforeach
						@endif
					</div>
				@endif
			@endforeach			
					<td class="text-center"><button type="submit1" class="btn btn-md btn-success">ORDER NOW</button></td>
			</form>
			</tr>

			<!-- Xeon E3 1270v5 Quad Core Server -->

			<tr>
			<form name="myform" id="myform"   action="{{route('cart.add_dedicated_server')}}" >  
			  <td>Xeon E3 1270v5 Quad Core Server</td>
			  <td>16GB</td>
			  <td>5TB</td>
			  <td>1TB SATA/120 GB SSD</td>
			  <td>1 IP</td>	
				<?php //echo "<pre>";print_r($products['products']['product']);exit; ?>
				@foreach($products['products']['product'] as $value)
			 		 @if($value['gid'] == "6")   
							@if($value['name'] == "Xeon E3 1270v5 Quad Core Server")
								@foreach($value['pricing'] as $cur => $key) 
								@if($cur==$currency)  
								<input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
															<input type="hidden" name="name1" value="{{$value['name']}}" />
															<input type="hidden" name="msetupfee" value="{{$key['msetupfee']}}" />
															<input type="hidden" name="price" value="{{$key['monthly']}}" />
															<input type="hidden" name="gid" value="{{$value['pid']}}" />
															<input type="hidden" name="description" value="{{$value['description']}}" />

									@foreach($value['configoptions']['configoption'] as $configure)                                  
										@if($configure['name']=='OS')
										<td>
											<div class="form-group">
												<select class="form-control price_change1" name="os" id="txt9" data-field="txt12" data-Pfield="txt10" data-Ffield="txt11" required>	
													<option value="">--select--</option>										
													@foreach($configure['options']['option'] as $options)   
														<option value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
													@endforeach
												</select>
											</div>									
										</td>
										@elseif($configure['name']=='Database')
									<td>									
										<div class="form-group">
											<select class="form-control price_change target" name="db" id="txt10" data-field="txt12" data-Pfield="txt9" data-Ffield="txt11">
									
												@foreach($configure['options']['option'] as $options)   
													<option value='{{$options["pricing"][$currency]["monthly"]}}-{{$options["name"]}}-{{$configure["id"]}}_{{$options["id"]}}'>{{$options["name"]}}</option>
												@endforeach
											</select>
										</div>
								</td>
									@endif
							@endforeach						
						<td>
							<input class="form-control" id="txt11" type='hidden' value="{{$key['monthly']}}" />
							<input class="form-control" id="txt12" name="price" type='text' value="<?php if($currency=="INR") { echo "₹";	}	else { echo "$";} ?>{{$key['monthly']}}" readonly/>
						</td>
						@endif
					@endforeach
				@endif
			</div>
		@endif
	@endforeach	
			  <td class="text-center"><button type="submit" class="btn btn-md btn-success">ORDER NOW</button></td>
			</form>
			</tr>
		</tbody>
	</table>
	</form>
</div>
</div>
  </div>
</div><!-- Dedicated Server  : Table End-->







<!-- Dedicated Server Features Section-->	
<section class="dedicated_features">
<div class="container text-center">
  <h2><b>FEATURES</b></h2>         
	  <table class="table table-bordered ">
		<thead>
		  <tr>
			<th style="font-size:19px;background-color: #07539e;color:white;"><b>Features</b></th>
			<th class="bg-dark text-white" style="font-size:19px;">Dedicated Server</th>
		  </tr>
		</thead>
		<tbody>
		  <tr>
			<td class="features">Web site hosting</td>
			<td><strong>$10</strong></td>
		  </tr>
		  <tr>
			<td class="features">Root Access</td>
			<td><img  src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features1"></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">24x7 Support</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Optional Support</td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}} " height="30px" alt="dedicated_features2" ></td>
		  </tr>
		  <tr>
			<td class="features" style="color:#07539e">Free SSL</td>
			<td><strong>YES</strong></td>
		  </tr>
		  <tr>
			<td class="features">Dedicated hosting</td>
			<td><strong>$100</strong></td>
		  </tr>			  </tr>
		  <tr>
			<td class="features">Free Website Migration
				<a href="#" data-toggle="tooltip" title="Free website migration is available on Dedicated Server purchased for a minimum 3 months tenure." style="color: #337ab7;
				 text-decoration: none;cursor:point;background-color: transparent;"><span class="glyphicon glyphicon-info-sign"></span>
				 <img src="{{asset('img/ico/Information.png')}}" alt="Information.png" >
				 </a></td>
			<td><img src="{{asset('img/ico/dedicated_features2.png')}}" height="30px" alt="dedicated_features2" ></td>
		  </tr>
		</tbody>
	  </table>
</div>
</section>

<script type='text/javascript'>

$(".target option:first").attr('selected','selected');

		 $(".price_change").change(function(){
       
       var field=$(this).attr('data-field');
	   var Pfield=$(this).attr('data-Pfield');
	   var Ffield=$(this).attr('data-Ffield');

	   var val=parseFloat($(this).val());
	   var Pval=parseFloat($('#'+Pfield).val());
	   var Fval=parseFloat($('#'+Ffield).val());
	   var total=parseFloat(val+Pval+Fval);
	   if (!isNaN(total)) {
	   document.getElementById(field).value = parseFloat(total); }

});
$(".price_change1").change(function(){
	var field=$(this).attr('data-field');
	   var Pfield=$(this).attr('data-Pfield');
	   var Ffield=$(this).attr('data-Ffield');

	   var val=parseFloat($(this).val());
	   var Pval=parseFloat($('#'+Pfield).val());
	   var Fval=parseFloat($('#'+Ffield).val());
	   var total=parseFloat(val+Pval+Fval);
	   if (!isNaN(total)) {
	   document.getElementById(field).value = parseFloat(total); }
});

		 $(function()
{
    $("#myform").validate(
      {
        rules: 
        {
          os: 
          {
            required: true
          }
        }
      });	
});	 
		

        
</script>

@endsection
