<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
  
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
    <script src="{{asset('js1/jquery-3.2.1.slim.min.js')}}"></script>
    <script src="{{asset('js1/bootstrap.min.js')}}"></script>
    <script src="{{asset('js1/decksysApp-v2.js')}}"></script>
    <script src="{{asset('js1/vps-validation.js')}}"></script>   
    <!--Custom Stylesheets-->
    <link rel="stylesheet" href="{{asset('css/style-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/mq-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/fs-v2.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{asset('js1/popper.min.js')}}"></script>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>    
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
	<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>	

  <link rel="stylesheet" href="{{asset('css/fs-v2.css')}}" type="text/css" />
    
    <script src="{{asset('js1/StatesDropdown.js')}}"></script> 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
    @yield('styles')
</head>
<body>
@include('partials.header')

@yield('content')

@include('partials.footer')

@yield('scripts')

</body>
</html>
