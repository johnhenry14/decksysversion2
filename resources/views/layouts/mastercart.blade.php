<!doctype html>
<html class="no-js" lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>@yield('title')</title>

    <link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
     
    <link rel="stylesheet" href="{{asset('css/style-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/mq-v2.css')}}">
    <link rel="stylesheet" href="{{asset('css/fs-v2.css')}}">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src="{{asset('js1/popper.min.js')}}"></script>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js'></script>    
	<link rel="stylesheet" href="http://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css" type="text/css" />
	<!-- <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.1.1/css/bootstrap.css" type="text/css" /> -->
    
  
    <link rel="stylesheet" href="{{asset('css/fs-v2.css')}}" type="text/css" />

    <script src="{{asset('js1/StatesDropdown.js')}}"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> 
    

    <style>
        .invalid-feedback1 {
            margin-top: .25rem;
            font-size: .875rem;
            color: #dc3545;
        }
    .req{
  color:red;
}
.center-block {
  display: block;
  margin-right: auto;
  margin-left: auto;
}.custom{
    color:#fff;
}
.anchor {
  color: #fff !important;
}

 

    </style>
    


    @yield('styles')
</head>
<body>
<!-- <div class="loader"></div> -->
@include('partials.headerCart')



@yield('content')

@include('partials.footer')

@yield('scripts')


<script> (function() {
    'use strict';
    window.addEventListener('load', function() {
      var form = document.getElementById('needs-validation');
      form.addEventListener('submit', function(event) {
        if (form.checkValidity() === false) {
          event.preventDefault();
          event.stopPropagation();
        }
        form.classList.add('was-validated');
      }, false);
    }, false);
  })();

  //numbers and textonly//
  function isNumberKey(evt){  <!--Function to accept only numeric values-->
    //var e = evt || window.event;
	var charCode = (evt.which) ? evt.which : evt.keyCode
    if (charCode != 46 && charCode > 31
	&& (charCode < 48 || charCode > 57))
        return false;
        return true;
	}

    function ValidateAlpha(evt)
    {
        var keyCode = (evt.which) ? evt.which : evt.keyCode
        if ((keyCode < 65 || keyCode > 90) && (keyCode < 97 || keyCode > 123) && keyCode != 32)

        return false;
            return true;
    }

  </script>

 <script>

     var password = document.getElementById("password")
         , confirmpassword = document.getElementById("confirmpassword");

     function validatePassword(){
         if(password.value != confirmpassword.value) {
             confirmpassword.setCustomValidity("Passwords Don't Match");
         } else {
             confirmpassword.setCustomValidity('');
         }
     }

     password.onchange = validatePassword;
     confirmpassword.onkeyup = validatePassword;

 </script>

<script>
$(document).ready(function() {
    $(".loader").fadeOut(3000);
});
    $(document).ready(function() {

    $('input[type=password]').keyup(function() {
        var pswd = $(this).val();
        //validate the length
if ( pswd.length < 8 ) {
    $('#length').removeClass('valid').addClass('invalid');
} else {
    $('#length').removeClass('invalid').addClass('valid');
}
//validate letter
if ( pswd.match(/[A-z]/) ) {
    $('#letter').removeClass('invalid').addClass('valid');
} else {
    $('#letter').removeClass('valid').addClass('invalid');
}

//validate capital letter
if ( pswd.match(/[A-Z]/) ) {
    $('#capital').removeClass('invalid').addClass('valid');
} else {
    $('#capital').removeClass('valid').addClass('invalid');
}

//validate number
if ( pswd.match(/\d/) ) {
    $('#number').removeClass('invalid').addClass('valid');
} else {
    $('#number').removeClass('valid').addClass('invalid');
}

if(pswd.match(/[@$,<>#:?_*&;]/))
{
    $('#special').removeClass('invalid').addClass('valid');
    
}
else
{
    $('#special').removeClass('valid').addClass('invalid');
}
}).focus(function() {
    $('#pswd_info').show();
}).blur(function() {
    $('#pswd_info').hide();
});
});
    
    </script>


</body>
</html>
