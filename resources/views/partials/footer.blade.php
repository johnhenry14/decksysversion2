 <footer class="appFooter">
        <section class="footerSec1 p-4">
            <div class="container-fluid text-center text-md-left ">
                <div class="row">
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <h4>Quick links</h4>
                        <hr>
                        <ul class="footerLinks" >
                            <li><a href="/VPS">SSD VPS</a></li>
                            <li><a href="/Dedicated_Server">Dedicated Server</a></li>
                            <li><a href="/ManagedServices">Managed Services</a></li>
                            <li><a href="/support">Support</a></li>
                            
                        </ul>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <div class="socialConnect">
                            <h4>Find us on Social</h4>
                            <hr>
                            <ul class="socialItems">
                            <li><a href="https://www.facebook.com/decksysinf"><img class="media-object" src="{{asset('img/ico/fb-icon.png')}}"   alt="fb"></a></li>
                                <li><a href="https://twitter.com/decksysinf"><img class="media-object" src="{{asset('img/ico/twitter-icon.png')}}"  alt="tw"></a></li>
                                <li><a href="https://www.linkedin.com/company/decksys"><img class="media-object" src="{{asset('img/ico/linkedin-icon.png')}}"  alt="In"></a></li>

                            </ul>
                        </div>
                    </div>
                    <div class="col-12 col-sm-12 col-md-4 col-lg-4">
                        <h4>Contact Address</h4>
                        <hr>
                        <div class="footerContact">
                            <div class="media">
                                <div class="media-left ">
                                    <a href="#">
                                    <img class="media-object" src="{{asset('img/logo/logo-makto.png')}}"  alt="...">
                                    </a>
                                </div>
                                <div class="media-body pl-3 mr-5">
                                    <h5 class="text-center">Makto Technology Pvt Ltd</h5>
                                        <p class="text-center" >B2, First Floor, Rathinam Technical Campus,
                                            Pollachi Main Road, Eachanari,
                                            Coimbatore-641021.
                                        </p>   
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>    
            
            <section class="footerSec2 pt-4">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-12 col-sm-12 col-md-8 col-lg-12">
                            <div class="copyrights">
                                <ul class="policy">
                                    <li><a href="Privacy">Privacy Policy</a></li>
                                    <li><a href="Terms">Terms and Conditions</a></li>
                                    <li><a href="Refund">Refund Policy</a></li>
                                </ul>
                                <div>
                                    <ul class="copyright">
                                        <li>
                                            <p>Copyrights 2019. Makto Technology Pvt Ltd.</p>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </footer>
    <script>
    $.ajaxSetup({
  headers: {
    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
  }
});
         $('#currency_change').change(function(e){
             var currency=$(this).val();
            $.ajax({
                headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
          },
               type:'POST',
               url:'{{route("currency_change")}}',
               data:'currency='+currency+'&_token = <?php echo csrf_token() ?>',
               success:function(data) {
                location.reload();
               }
            });
         });
            
    </script>
    	<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "917397767219", // WhatsApp number
            //call_to_action: "Message us", // Call to action
            position: "left", // Position may be 'right' or 'left'
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
<!-- /WhatsHelp.io widget -->

    <script type="text/javascript">window.$crisp=[];window.CRISP_WEBSITE_ID="3e55bc02-2699-4c82-970d-160c09b96dfd";(function(){d=document;s=d.createElement("script");s.src="https://client.crisp.chat/l.js";s.async=1;d.getElementsByTagName("head")[0].appendChild(s);})();</script>
