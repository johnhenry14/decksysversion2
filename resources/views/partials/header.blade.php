<meta name="csrf-token" content="{{ csrf_token() }}">
<header class="appHeader">
<div class="navOne">
    <div class="container-fluid"> 
        <div class="row no-gutters">
            
            <div class="col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                <div class="userServices" data-toggle="modal" data-target="#requestQuoteModal">
                    <img src="{{asset('img/ico/phone.png')}}" alt="Get_Quote">
                    <p>+91 84484 44086</p>
                </div>
               
            </div>
            
            
            <div class="col-2 col-sm-1 col-md-3 col-lg-2 col-xl-2">
                <div class="userServices" data-toggle="modal" data-target="#emailModal">
                    <img src="{{asset('img/ico/mail.png')}}" alt="Get_Quote">
                    <p>sales@decksys.com</p>
                </div>
              
            </div>
            <div class="col-2 col-sm-1 col-md-2">
                <div class="userServices" data-toggle="modal" data-target="#callUsModal">
                    <img src="{{asset('img/ico/chat.png')}}" alt="Get_Quote">
                    <p>Live Chat</p>
                </div>
               
            </div>
            <div class="col-2 col-sm-1 col-md-2 offset-md-3 pt-2">
            <form name="form">
                        <select name="currency" class="form-control" id="currency_change">
                            @foreach($currency['currencies']['currency'] as $key=>$value)
                                @for($key=0;$key<100;$key++)
                                @endfor
                                <option value="{{$value['code']}}" <?php if($value['code']==session()->get('currency')){ echo "selected"; } ?>> {{$v=$value['code']}}</option>
                            @endforeach
                        </select>
                    </form>
            </div>
            <div class="col-2 col-sm-1 col-md-2">
                <div class="userServices" data-toggle="modal" data-target="#loginModal" style="border:none;">
                    <img src="{{asset('img/ico/login.png')}}" alt="Get_Quote">
                    @php $var=session()->get('login_user_name') @endphp
                        @if($var=="")
                        <p class="hidden-sm hidden-md"><a href="clientlayout.main.index" class="text-dark">{{$var}}</a><a href="/signout"><i class="fa fa-sign-out text-primary pl-3" ></i> </a></p>
                        @else
                        <p><a class="text" href="Register">Register </a>/ <a class="text" href="/login">Login</a></p>
                            @endif
                </div>
            </div>
        </div>
    </div>
</div>
<div  class="navbarTwo">
    <nav class="navbar  navbar-expand-md navbar-light bgcolor">
      <a class="navbar-brand" href="/">
        <img src="{{asset('img/logo/logo-decksys.png')}}" alt="logo" class="img-fluid img-logo">
      </a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarText" aria-controls="navbarText"
        aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon "></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarText">
        <ul class="navbar-nav ml-md-auto d-md-flex">
          <li class="nav-item">
            <a class="nav-link text-white" href="/VPS">SSD VPS</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/DedicatedServer">Dedicated Server</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/ManagedServices">Managed Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/support">Support</a>
          </li>
          <li class="nav-item">
            <a class="nav-link text-white" href="/Reach">Reach Us</a>
          </li>
        </ul>
      </div>
    </nav>
</header>
<section class="carouselSection">
<div id="demo" class="carousel slide" data-ride="carousel">
    <!-- Indicators -->
    <ul class="carousel-indicators">
        <li data-target="#demo" data-slide-to="0" class="active"></li>
        <li data-target="#demo" data-slide-to="1"></li>
        <!-- <li data-target="#demo" data-slide-to="2"></li> -->
    </ul>
    <!-- The slideshow -->
    <div class="carousel-inner">
            
        <div class="carousel-item active">
            <img class="img-fluid" src="{{asset('img/Home/speedssd.jpg')}}"  alt="">
            <!-- <div class="carousel-caption ">
                <h3>Come To Do The Best Work.</h3>
                <p>We Help Our Clients Reinvent Themselves.</p>
            </div> -->
        </div>
        <div class="carousel-item">
            <img class="img-fluid" src="{{asset('img/Home/dataCenter.jpg')}}"  alt="" >
            <!-- <div class="carousel-caption">
                <h3>Come To Do The Best Work.</h3>
                <p>We Help Our Clients Reinvent Themselves.</p>
            </div> -->
        </div>
        <!-- <div class="carousel-item active">
                <img src="../resources/img/Home/slide_3.png" class="img-fluid" alt="" >
            </div> 
    </div> -->
    <!-- Left and right controls -->
    <a class="carousel-control-prev" href="#demo" data-slide="prev">
        <span class="carousel-control-prev-icon"></span>
    </a>
    <a class="carousel-control-next" href="#demo" data-slide="next">
        <span class="carousel-control-next-icon"></span>
    </a>
</div>
</section>