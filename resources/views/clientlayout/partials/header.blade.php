<div class="app app-header-fixed ">
<!-- header -->
<header id="header" class="app-header navbar" role="menu">
  <!-- navbar header -->
  <div class="navbar-header bg-dark">
    <button class="pull-right visible-xs dk" ui-toggle-class="show" target=".navbar-collapse">
      <i class="glyphicon glyphicon-cog"></i>
    </button>
    <button class="pull-right visible-xs" ui-toggle-class="off-screen" target=".app-aside" ui-scroll="app">
      <i class="glyphicon glyphicon-align-justify"></i>
    </button>
    <!-- brand -->
    <a class="navbar-brand" href="/">
      <img src="{{asset('client/img/logo-decksys.png')}}"  alt="Decksys" class="img-responsive">
    </a>
    <!-- / brand -->
</div>
<!-- / navbar header -->
<!-- navbar collapse -->
  <div class="collapse pos-rlt navbar-collapse box-shadow bg-white-only">
    <!-- buttons -->
    <div class="nav navbar-nav hidden-xs">
      <a href="#" class="btn no-shadow navbar-btn" ui-toggle-class="app-aside-folded" target=".app">
        <i class="fa fa-dedent fa-fw text"></i>
        <i class="fa fa-indent fa-fw text-active"></i>
      </a>
  </div>
  <!-- / buttons -->
  <!-- link and dropdown -->
    <ul class="nav navbar-nav hidden-sm">
      <li class="dropdown pos-stc" style="background-color: #a1e1ff;">
        <a href="http://projects.maktoinc.com/kb/" target="_blank">
          <span>Knowledge Base</span> 
        </a>
      </li>
      <li class="dropdown" style="background-color: #bababa;">
        <a href="#" data-toggle="dropdown" class="dropdown-toggle">
          <i class="fa fa-fw fa-plus visible-xs-inline-block"></i>
          <span translate="header.navbar.new.NEW">Support : </span>
          <span> +91 - 73977 67219 </span>
        </a>
      </li>
    </ul>
    <!-- / link and dropdown -->
      <!-- nabar right -->
      <ul class="nav navbar-nav navbar-right">
        <li class="dropdown">
          <a href="#" data-toggle="dropdown" class="dropdown-toggle clear" data-toggle="dropdown">
            <span class="thumb-sm avatar pull-right m-t-n-sm m-b-n-sm m-l-sm">
              <img src="{{asset('client/img/a0.png')}}" alt="...">
            </span>
            <span class="hidden-sm hidden-md">{{session()->get('login_user_name')}}</span> <b class="caret"></b>
          </a>
          <!-- dropdown -->
          <ul class="dropdown-menu animated fadeInRight w">
            <li><a ui-sref="app.page.profile" href="Myprofile">My Profile</a></li>
            <li class="divider"></li>
            <li><a ui-sref="app.page.profile" href="Password">Change Password</a></li>
            <li class="divider"></li>
            <li><a ui-sref="app.page.profile" href="Mycontacts">Contacts</a> </li>
            <li class="divider"></li>
            <li><a href="{{ route('logout.signout') }}" ><i class="fa fa-sign-out"></i> Logout</a></li>
            
          </ul>
          <!-- / dropdown -->
        </li>
      </ul>
    <!-- / navbar right -->
  </div>
  <!-- / navbar collapse -->
</header>
<!-- / header -->

<!-- aside -->
<aside id="aside" class="app-aside hidden-xs bg-dark">
  <div class="aside-wrap">
    <div class="navi-wrap">
      <!-- nav -->
      <nav ui-nav class="navi clearfix">
        <ul class="nav">
          <li>
            <a href="clientlayout.main.index" class="auto">      
              <i class="fa fa-home"></i>
              <span class="font-bold">Dashboard</span>
            </a>
          </li>
          <li>
            <a href="Mydomains">  
              <span class="pull-right text-muted">
                <i class="fa fa-fw fa-angle-right text"></i>
                <i class="fa fa-fw fa-angle-down text-active"></i>
              </span>    
              <i class="fa fa-globe"></i>
                <span>My Domain</span>
            </a>
          </li>
          <li>
            <a href class="auto">      
              <span class="pull-right text-muted">
                <i class="fa fa-fw fa-angle-right text"></i>
                <i class="fa fa-fw fa-angle-down text-active"></i>
              </span>
              <i class="fa fa-wrench"></i>
                <span>Services</span>
            </a>
            <ul class="nav nav-sub dk">
              <li>
                <a href="Services">
                  <span>My Services</span>
                </a>
              </li>
              <li>
                <a href>
                  <span>New Services</span>
                </a>
              </li>
            </ul>
          </li>
          <li>
            <a href class="auto">      
              <span class="pull-right text-muted">
                <i class="fa fa-fw fa-angle-right text"></i>
                <i class="fa fa-fw fa-angle-down text-active"></i>
              </span>
              <i class="fa fa-money"></i>
                <span>Accounts</span>
            </a>
            <ul class="nav nav-sub dk">
              <li>
                <a href="Orders">
                  <span>Orders</span>
                </a>
              </li>
              <li>
                <a href="Invoices">
                  <span>Payments & Invoices</span>
                </a>
              </li>
              <li>
                  <a href="Funds">
                    <span>Funds</span>
                  </a>
              </li>  
            </ul>
          </li>
            
          <li>
            <a href class="auto">      
              <span class="pull-right text-muted">
                <i class="fa fa-fw fa-angle-right text"></i>
                <i class="fa fa-fw fa-angle-down text-active"></i>
              </span>
              <i class="glyphicon glyphicon-th"></i>
              <span>Resolution Center</span>
            </a>
            <ul class="nav nav-sub dk">
              <li>
                <a href="Tickets">
                  <span>My Tickets</span>
                </a>
              </li>
              <li>
                <a href="RaiseTicket">
                  <span>Raise an Issue</span>
                </a>
              </li>
              <li>
                <a href="http://projects.maktoinc.com/kb/" target="_blank">
                  <span>Knowledge Base</span>
                </a>
              </li>  
            </ul>
          </li>
           
           
          </ul>
          <!-- <a href="" class="btn btn-info btn-sm" style="padding: 10px 20px 10px 20px; margin: 20px 35px;">Build your server</a> -->
        
        </nav>
        <!-- nav -->

       
      </div>
    </div>
</aside>
<!-- / aside -->
