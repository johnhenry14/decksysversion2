@extends('clientlayout.layouts.master')

@section('title')
 Decksys | Tickets 
@endsection

@section('content')

<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h1 block">Tickets</h1>
</div>
<div class="wrapper-md">
  <div class="panel panel-default">
   
    <div class="table-responsive">
      <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper form-inline dt-bootstrap no-footer">
<div class="row"><div class="col-sm-12"><table ui-jq="dataTable" ui-options="{
          sAjaxSource: 'api/datatable.json',
          aoColumns: [
            { mData: 'engine' },
            { mData: 'browser' },
            { mData: 'platform' },
            { mData: 'version' },
            { mData: 'grade' }
          ]
        }" class="table table-striped b-t b-b dataTable no-footer" id="DataTables_Table_0" role="grid" aria-describedby="DataTables_Table_0_info">
        <thead>
          <tr role="row">
          <th class="sorting_desc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="descending">S.No</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">Ticket Id</th>
          <th class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">Name</th>

          <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Subject</th>
          <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Priority</th>
          <th  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">Status</th>
         </tr>
        </thead>
        <tbody>
        @if($results['totalresults'] == '0')
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
          <td> - </td>
        @else
         
          @foreach($results['tickets']['ticket'] as $value)

        
        <tr role="row">
          <td  class="sorting_asc" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Rendering engine: activate to sort column ascending" aria-sort="ascending">{{$value['id']}}</td>
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Browser: activate to sort column ascending">{{$value['tid']}}</td>

          
          <td  class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Platform(s): activate to sort column ascending">{{$value['name']}}</td>

          <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$value['subject']}}</td>
          <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">{{$value['priority']}}</td>
          
          <td class="sorting" tabindex="0" aria-controls="DataTables_Table_0" rowspan="1" colspan="1" aria-label="Engine version: activate to sort column ascending">
          
          @if ($value['status'] == "Answered")
                  <a href="/ReplyTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-success btn-sm">{{$value['status']}}</button></a>
                  <a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                @elseif($value['status']=="Closed")
                  <button class="btn btn-primary btn-sm" style="padding: 5px 20px 5px 20px;">{{$value['status']}}</button>
<a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

                  @elseif($value['status']=="Open")
                  <a href="/ReplyTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-warning btn-md" style="padding: 5px 20px 5px 20px;">{{$value['status']}}</button></a>
<a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
                  @elseif($value['status']=="On Hold")
                  <a href="/ReplyTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-secondary btn-sm" style="padding: 5px 20px 5px 20px;">{{$value['status']}}</button></a>
<a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

                  @elseif($value['status']=="In Progress")
                  <a href="/ReplyTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-info btn-sm" style="padding: 5px 20px 5px 20px;">{{$value['status']}}</button> </a>            
<a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></a>

                  @elseif($value['status']=="Customer-Reply")
                  <a href="/ReplyTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-info btn-sm" style="padding: 5px 16px 5px 16px;">Replied</button> </a>            
<a href="/DeleteTicket?ticket_id=<?=Crypt::encrypt($value['id']);?>"><button class="btn btn-danger btn-sm"><i class="fa fa-trash" aria-hidden="true"></i></button></a>
              
          @endif

         </tr>
         @endforeach
   
        
        @endif
        
        </tbody>
      </table></div></div>
      </div>
    </div>
    
  </div>
  
  



 

 


	</div>
  </div>
  </div>


@endsection