@extends('clientlayout.layouts.master')

@section('title')
	Decksys | Create Ticket
@endsection

@section('content')


<?php //echo "<pre>";print_r($openticket);exit; ?>

<!-- content --> 
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Raise Your Ticket</h1>
</div>
<div class="wrapper-md" ng-controller="FormDemoCtrl">
  <div class="row">
   
    <div class="col-sm-12">
      <div class="panel panel-default">
 @if (Session::has('message'))
        <p style="text-align: center;font-size: 18px;" class="text-success">Ticket Created Successfully !</p>
        @endif
        <div class="panel-body col-lg-offset-2">
          <form class="bs-example form-horizontal" action="{{route('ticket.create')}}" method="post" enctype="multipart/form-data">
          <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
            
            <div class="form-group">
              <label class="col-lg-2 control-label">Subject</label>
              <div class="col-lg-6">
                <input type="text" class="form-control" name="subject" placeholder="Subject">
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Department</label>
              <div class="col-lg-6">
                <select class="form-control" name="deptid">
                 
                    <option value="1">sales</option>
                    <option value="2">Support</option>
                </select>
          
              </div>
            </div>
            <div class="form-group">
              <label class="col-lg-2 control-label">Priority</label>
              <div class="col-lg-6">
                <select class="form-control" name="priority">
                <option value="High">High</option>
                <option value="Medium">Medium</option>
                <option value="Low">Low</option></select>
              </div>
            </div>
			
		<div class="form-group">
              <label class="col-lg-2 control-label">Service Id</label>
              <div class="col-lg-6">
			  
                <select class="form-control" name="serviceid">
				@foreach($openticket['products']['product'] as $key => $value)
					@for($key=0;$key<100;$key++)
					@endfor
					<option value="{{$value['id']}}">{{$value['translated_name']}} -{{$value['domain']}} </option>
				@endforeach
                </select>
                
               
              </div>
            </div>
			
			
			<!--<input type="file" name="attachments" />-->
            

            <div class="form-group">
              <label class="col-lg-2 control-label" >Description</label>
              <div class="col-lg-6">
              <textarea rows="4" cols="50" class="form-control" name="message"></textarea>
              </div>
            </div>
           
            <div class="form-group">
              <div class="col-lg-10">
                <button type="submit" class="btn btn-sm btn-success center-block" style="margin-left: 400px;">Submit Ticket</button>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
  
	</div>
  </div>
  <!-- /content -->

@endsection