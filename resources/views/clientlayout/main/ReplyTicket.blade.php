@extends('clientlayout.layouts.master')

@section('title')
  Decksys | ReplyTicket
@endsection
@section('content')

<style>
.chat
{
    list-style: none;
    margin: 0;
    padding: 0;
    width:70%;
    margin:50px auto;
}
.chat li
{
    margin-bottom: 10px;
    padding-bottom: 5px;
    border-bottom: 1px dotted #B3A9A9;
}
.chat li.left .chat-body
{
    margin-left: 60px;
}
.chat li.right .chat-body
{
    margin-right: 60px;
}
.chat li .chat-body p
{
    margin: 0;
    color: #777777;
}
</style>
  
<div id="content" class="app-content" role="main">
  	<div class="app-content-body ">
	    

<div class="bg-light lter b-b wrapper-md">
  <h1 class="m-n font-thin h3">Reply Ticket</h1>
  <ul class="chat">

  @foreach($reply['replies']['reply'] as $value)
  <?php if($value['userid']==$clientid) { 
    $first_letter=substr($value['name'],0,4);
    $temp = explode(' ',$value['date']);
            
    ?>
  <li class="left clearfix"><span class="chat-img pull-left">
  <img src="http://placehold.it/40/55C1E7/fff&text=<?=$first_letter;?>" alt="User Avatar" class="img-circle" />
</span>
  <div class="chat-body clearfix">
      <div class="header">
          <p class="primary-font">{{$value['name']}}</p> <small class="pull-right primary-font">
         <i class="fa fa-calendar" aria-hidden="true"></i>
{{ $temp[0] }}
<br><i class="fa fa-clock-o" aria-hidden="true"></i>
{{ $temp[1] }}



</small>


      </div>
      <strong class="text-info">
      {{$value['message']}}
      </strong>
  </div>
</li>
    
  <?php } else { 
     $first_letter=substr($value['admin'],0,5);
    ?>
  <li class="right clearfix"><span class="chat-img pull-right">
  <img src="http://placehold.it/40/7266ba/fff&text=<?=$first_letter;?>" alt="User Avatar" class="img-circle display-1" />
                        </span>
                            <div class="chat-body clearfix">
                                <div class="header">
                                    <small class="primary-font"><i class="fa fa-calendar" aria-hidden="true"></i>
{{ $temp[0] }}<br>
<i class="fa fa-clock-o" aria-hidden="true"></i>
{{ $temp[1] }}
</small>
                                    <p class="pull-right primary-font">{{$value['admin']}}</p>
                                </div>
                                <strong class="pull-right primary-font text-primary">
                                {{$value['message']}}
                                </strong>
                            </div>
                        </li>
    
  <?php } ?>    
                           
  @endforeach
  </ul>
</div>

<div class="wrapper-md" ng-controller="FormDemoCtrl">
  <div class="panel panel-default">
    <div class="panel-body">
      <form class="form-horizontal" action="{{route('ticket.add_reply')}}" method="post">
      <input type="hidden" name="ticket_id" value="<?=$ticket_id?>" />
      <input type="hidden" name="user_id" value="<?=$clientid?>" />
      <input type = "hidden" name = "_token" value = "<?php echo csrf_token(); ?>">
        <div class="form-group">
          
          <div class="col-sm-offset-2 col-sm-8">
 
            
                  <textarea class="form-control no-border" rows="3" name="message" placeholder="Your comment..."></textarea>
                         </div>
        </div>
        
      
        <div class="form-group">

          <div class="col-sm-4 col-sm-offset-6">
               <button class="btn btn-info pull-right btn-sm">Comment</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>



	</div>
  </div>





@endsection