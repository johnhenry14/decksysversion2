<?php

use DarthSoup\Whmcs\Facades\Whmcs;

use Illuminate\Support\Facades\View;

use Darthsoup\Whmcs\WhmcsServiceProvider;

use Razorpay\Api\Api;

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/




Route::get('/ManagedServices', function () {
    return view('main.ManagedServices');
});

Route::get('/support', function () {
    return view('main.support');
});

Route::get('/locations', function () {
    return view('main.locations');
});

Route::get('/Register', function () {
    return view('main.Register');
});

Route::get('/login', function () {
    return view('main.login');
});

Route::any('/add_to_cart_dedicated_server', 'CartController@addCart_dedicated')->name('cart.add_dedicated_server');
Route::any('/add_to_cart_vps_server', 'CartController@addCart_vps')->name('cart.add_vps_server');
Route::any('/cart', 'CartController@cart_view')->name('cart');
Route::get('/checkout', 'PaymentController@payWithRazorpay')->name('payWithRazorpay.payWithRazorpay');
Route::get('/payment_invoice/{id}/{id1}', 'PaymentController@invoice_details')->name('payment.invoice');
Route::any('/payment_success', 'PaymentController@payment_success')->name('payment.success');
Route::any('/apply_promo', 'CartController@apply_promo')->name('cart.apply_promo');
Route::any('/delete_promo', 'CartController@delete_promo')->name('cart.delete_promo');
Route::any('/cart_remove', 'CartController@cart_remove')->name('cart.remove');
Route::any('/destroy', 'CartController@destroy');
Route::any('/VPS', 'ProductController@show');
Route::any('/DedicatedServer', 'ProductController@show2');
Route::any('/', 'ProductController@home');
Route::post('currency_change', 'ProductController@CurrencyChange')->name('currency_change');

/* ------------- Registration and Login system ------------------*/

Route::post('insert','RegisterController@insert')->name('user.insert');
Route::any('login', 'LoginController@signin');
Route::post('signin_action','LoginController@signin_action')->name('login.signin_action');
Route::get('signout', 'LoginController@signout')->name('logout.signout');
Route::get('/reset_password','LoginController@reset_password');
Route::post('reset_password_action','LoginController@reset_password_action')->name('login.reset_password_action');
Route::post('forgotpassword_action','LoginController@forgotpassword_action')->name('login.forgotpassword_action');
Route::get('/Myprofile','ProfileController@profile_update_action')->name('profile.profile_update_action');
Route::post('profile_update_action_update','ProfileController@profile_update_action_update')->name('profile.profile_update_action_update');
Route::get('Password', function () {
    return view('clientlayout.main.Password');
});

Route::get('/forgotpassword', function () {
    return view('main.forgotpassword');
});


/* ------------- Ticketing system ------------------*/

Route::post('create','CreateTicketController@create')->name('ticket.create');
Route::get('Tickets','CreateTicketController@show_tickets')->name('results.show_tickets');
Route::get('ReplyTicket','CreateTicketController@reply_ticket')->name('reply.reply_ticket');
Route::get('DeleteTicket','CreateTicketController@DeleteTicket')->name('reply.DeleteTicket');
Route::post('add_ticket_reply','CreateTicketController@add_reply')->name('ticket.add_reply');
Route::get('RaiseTicket','OpenTicketController@clientproduct')->name('ticket.clientproduct');

/* ------------- Client Portal ------------------*/

Route::get('/clientlayout.main.index','IndexController@get');
Route::get('/Myservices','MyServicesController@show');
Route::get('/Services','IndexController@show');
Route::get('/Mydomains','IndexController@getdomains');
Route::get('/Invoices','IndexController@payment');
Route::get('/Orders','IndexController@orders');
Route::any('/Funds', 'FundsController@Funds')->name('Funds.success');

Route::get('contact', 'ContactController@create')->name('contact.create');
Route::post('contact', 'ContactController@store')->name('contact.store');

Route::post('ajax_control_panel', 'ProductController@ajax_cp')->name('ajax_control_panel');

Route::get('Mycontacts', function () {
    return view('clientlayout.main.Mycontacts');
});

Route::get('Privacy', function () {
    return view('main.Privacy');
});
Route::get('Reach', function () {
    return view('main.Reach');
});
Route::get('Terms', function () {
    return view('main.Terms');
});
Route::get('Refund', function () {
    return view('main.Refund');
});
